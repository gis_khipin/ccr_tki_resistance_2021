# Codes and data to reproduce figures in CCR TKI-resistance paper

This repository contains R codes to reproduce the figures published in the CCR 
manuscript. Each folder is named according to the corresponding figures in the 
manuscript and the R scripts are contained inside the respective folders. Note 
that the versions of the packages used at the point of the analysis can be found 
in the file `./R_packages_CCR_code_upload_2021-6-18.tsv`. Common data used in 
many scripts are in the folder `data_file`. `misc_codes` contains some common 
references and helper functions sourced in the analysis.
