#' 2021-6-18: Cleaned up code for CCR publication upload

library(here)
library(tidyverse)
library(cowplot)
library(ggpubr)
library(ggbeeswarm)
library(ggrepel)
library(ggstatsplot)

theme_set(theme_bw(base_size = 18))

source("../misc_codes/R_common_func.R")
source("plot_gene_function.R")
source("../misc_codes/copy_number/copy_number_functions.R")

drivers <- load_drivers()

# Log FPKM expression for RNA-seq
logfpkm <- read_csv("../data_file/impact_expression_fpkm.csv")
samples_masterlist <- read_tsv("../data_file/samples_masterlist.tsv") %>% 
  filter(Cohort=="IMPACT", RNA_Seq=="Yes", 
         # Only use lung tissue samples to avoid tissue of origin from affecting expression
         sample_biopsySite == "Lung")

# Use imputed purities from large cohort based on multiple purities estimate. Purity imputation code
# can be found in the folder "purity_imputation"
imputed_purity <- read_tsv("purity_imputation/mean_purity_estimates_v5.tsv", skip = 1, col_names = c("Tumor_Sample_Barcode", "imputed_purity"))
imputed_purity <- imputed_purity %>% 
  mutate(Tumor_Sample_Barcode = gsub("GIS-(.*)-E", "\\1", Tumor_Sample_Barcode))
samples_masterlist <- samples_masterlist %>% 
  inner_join(imputed_purity, by="Tumor_Sample_Barcode")
samples_masterlist$purity <- samples_masterlist$imputed_purity

# Remove tumor not from lung tissue or histologically different (See manuscript's method)
samples_nohisto <- samples_masterlist %>% 
  filter(!grepl("A092|A450|A465|A038", Tumor_Sample_Barcode)) %>% 
  pull(Tumor_Sample_Barcode)

logfpkm <- logfpkm %>% 
  select(rsem_geneNames, samples_masterlist$Tumor_Sample_Barcode)
logfpkm_remove_histo <- logfpkm %>% 
  select(rsem_geneNames, samples_nohisto)

# Add cancer and stroma p-values from TUMERIC. Note that TURMERIC was carried out
# using imputed purities and *without* the histologically different samples (See manuscript method)
cancer_stroma <- read_csv("../data_file/TUMERIC_results.csv")

# Remove duplicated genes and keep the gene with higher MAD in case of duplicate
logfpkm_mat <- dedup_RNA_byMAD_toMatrix(logfpkm, samples_masterlist$Tumor_Sample_Barcode)

####################################################################################
# 2019-6-6: Regress genes against purity
####################################################################################
# Annotate histo
samples_masterlist <- samples_masterlist %>% 
  mutate(histology = if_else(Patient.ID %in% c("A092", "A038", "A450", "A465"), "SmallCell/Squamous", "Adeno"))
purities_df <- as.data.frame(samples_masterlist)
rownames(purities_df) <- samples_masterlist$Tumor_Sample_Barcode
# Without transformed samples
logfpkm_remove_transform <- logfpkm_mat[, which(!grepl("A092|A450|A465|A038", colnames(logfpkm_mat)))]

#############################################################################################
#' 2019-10-4: Plot NAPSA for manuscript illustration (Fig 2C)
#############################################################################################

napsa <- logfpkm_remove_histo %>% 
  filter(rsem_geneNames == "NAPSA")
napsa <- napsa %>% 
  gather(key="Tumor_Sample_Barcode", value="Log2FPKM", matches("A[0-9]")) %>% 
  inner_join(samples_masterlist, by="Tumor_Sample_Barcode")

# Load TUMERIC
tumeric_res <- read_csv("mut_wt_without_transformed_2019-6-12.csv")
napsa_res <- tumeric_res %>% filter(genes=="NAPSA")
napsa_reg <-
  tibble(T790M = c(rep("T790M+", 2), rep("T790M-",2)),
         x = c(1, 0, 1, 0),
         label = c("Cancer", "Stroma", "Cancer", "Stroma"),
         Log2FPKM = c(napsa_res$b_cancer_T790M_pos, napsa_res$b_stroma_T790M_pos,
                        napsa_res$b_cancer_T790M_neg, napsa_res$b_stroma_T790M_neg),
         error_max = c(napsa_res$b_cancer_T790M_pos + napsa_res$se_cancer_T790M_pos, 
                   napsa_res$b_stroma_T790M_pos + napsa_res$se_stroma_T790M_pos,
                   napsa_res$b_cancer_T790M_neg + napsa_res$se_cancer_T790M_neg, 
                   napsa_res$b_stroma_T790M_neg + napsa_res$se_stroma_T790M_neg),
         error_min = c(napsa_res$b_cancer_T790M_pos - napsa_res$se_cancer_T790M_pos, 
                       napsa_res$b_stroma_T790M_pos - napsa_res$se_stroma_T790M_pos,
                       napsa_res$b_cancer_T790M_neg - napsa_res$se_cancer_T790M_neg, 
                       napsa_res$b_stroma_T790M_neg - napsa_res$se_stroma_T790M_neg)) %>% 
  mutate(label_expr = paste(label, signif(Log2FPKM, 2), sep=": "))
# If error goes down to negative, set to 0
napsa_reg <- napsa_reg %>% 
  mutate(error_min = if_else(error_min <= 0, 0, error_min))

# Beautify the plot labelling so ggrepel will avoid overlapping with the points
points_to_avoid <- tibble(
  x = c(rep(seq(0,1, length.out = 20), 2),
        filter(napsa, T790M == "T790M+")[, "purity", drop=TRUE],
        filter(napsa, T790M == "T790M-")[, "purity", drop=TRUE]),
  Log2FPKM = c(seq(filter(napsa_reg, T790M == "T790M+", label == "Stroma")[, "Log2FPKM", drop=TRUE],
            filter(napsa_reg, T790M == "T790M+", label == "Cancer")[, "Log2FPKM", drop=TRUE], 
            length.out = 20),
        seq(filter(napsa_reg, T790M == "T790M-", label == "Stroma")[, "Log2FPKM", drop=TRUE],
            filter(napsa_reg, T790M == "T790M-", label == "Cancer")[, "Log2FPKM", drop=TRUE], 
            length.out = 20),
        filter(napsa, T790M == "T790M+")[, "Log2FPKM", drop=TRUE],
        filter(napsa, T790M == "T790M-")[, "Log2FPKM", drop=TRUE]),
  T790M = c(rep(c("T790M+", "T790M-"), 20),
            as.character(filter(napsa, T790M == "T790M+")[, "T790M", drop=TRUE]),
            as.character(filter(napsa, T790M == "T790M-")[, "T790M", drop=TRUE])),
  label_expr = " "
)
points_to_avoid$T790M <- factor(points_to_avoid$T790M, levels=c("T790M+", "T790M-"))

library(ggrepel)
napsa$T790M <- factor(napsa$T790M, levels=c("T790M+", "T790M-"))
napsa_reg$T790M <- factor(napsa_reg$T790M, levels=c("T790M+", "T790M-"))
plot_napsa_withlabel <- ggplot(napsa, aes(x=purity, y=Log2FPKM)) +
  geom_point(size=4, alpha=0.75, aes(color=T790M)) +
  scale_x_continuous(expand = c(0, 0), limits = c(0, 1)) +
  # geom_smooth(method="lm", fullrange=TRUE, alpha=0.25, color="black") +
  geom_line(data = napsa_reg, aes(x=x, y=Log2FPKM)) +
  geom_errorbar(data = napsa_reg, aes(x=x, ymin=error_min, ymax=error_max), size=2, color="red") +
  geom_text_repel(aes(label=Patient.ID, color=T790M), nudge_y = 2) +
  facet_wrap(~ T790M) +
  labs(title = paste0("NAPSA", " Expression against Purity"), x="Tumor Purity") +
  theme(axis.text.x = element_text(angle=45, hjust=0.5, vjust=0.5),
        panel.spacing = unit(2, "lines"),
        legend.position="bottom")
ggsave("NAPSA_purity_withLabels.png", plot_napsa_withlabel, width=10, height=7)

# Remove samples labelling (manuscript figure)
plot_napsa <- ggplot(napsa, aes(x=purity, y=Log2FPKM)) +
  geom_point(size=4, alpha=0.75, aes(color=T790M)) +
  scale_x_continuous(expand = c(0, 0), limits = c(0, 1)) +
  scale_y_continuous(expand = c(0, 0), limits = c(0, 14)) +
  # geom_smooth(method="lm", fullrange=TRUE, alpha=0.25, color="black") +
  geom_line(data = napsa_reg, aes(x=x, y=Log2FPKM)) +
  geom_point(data = napsa_reg, aes(x=x, y=Log2FPKM), alpha=0.5, size=5) +
  geom_errorbar(data = napsa_reg, aes(x=x, ymin=error_min, ymax=error_max), 
                size=3, color="red", width=0.2, alpha=0.5) +
  geom_text_repel(data = bind_rows(napsa_reg, points_to_avoid), aes(x=x, y=Log2FPKM, label=label_expr),
                   hjust="inward", segment.colour = NA) +#, nudge_x = 0.05, nudge_y = 2, alpha=1) +
  # geom_label_repel(data = bind_rows(napsa_reg, points_to_avoid), aes(x=x, y=Log2FPKM, label=label_expr), 
  #                  hjust="inward", nudge_x = 0.05, nudge_y = 2, alpha=1, fill=NA) +
  facet_wrap(~ T790M) +
  labs(#title = paste0("NAPSA", " Expression against Purity"), 
       x="Tumor Purity", y="NAPSA Expression (Log2(FPKM + 1))") +
  theme(axis.text.x = element_text(angle=45, hjust=0.5, vjust=0.5),
        panel.spacing = unit(2, "lines"),
        legend.position="bottom")
ggsave("NAPSA_purity.pdf", plot_napsa, width=9, height=5.5)
