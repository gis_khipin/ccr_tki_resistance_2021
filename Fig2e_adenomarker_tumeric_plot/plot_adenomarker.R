#' 2021-6-18: Cleaned up code for CCR upload

library(tidyverse)
library(ReactomePA)
library(ggrepel)
library(ggsignif)
library(ggbeeswarm)
library(cowplot)
library(ggpubr)
source("../misc_codes/R_common_func.R")
source("../misc_codes/copy_number/copy_number_functions.R")

today_date <- Sys.Date()

theme_set(theme_bw(base_size = 18))

drivers <- load_drivers()

# Read TUMERIC deconvolution
pval_tumeric <- read_csv("../data_file/TUMERIC_results.csv")
# Some FDRs are zero, set to min p-values possible, 10e-5
pval_tumeric <- pval_tumeric %>% 
  mutate(c.pval = if_else(c.pval == 0, 10e-5, c.pval))
pval_tumeric$c.pval_adjusted <- p.adjust(pval_tumeric$c.pval, method="fdr")
# Calculate where FDR is equal to 0.25
fdr_0.25 <- pval_tumeric %>%
  arrange(c.pval) %>% 
  filter(c.pval_adjusted <= 0.25) %>% 
  pull(c.pval) %>% 
  nth(n=-1L)

adenomarkers <- c("NAPSA", "NKX2-1", "SFTA2", "SFTA3")

adenomarker_impact <- pval_tumeric %>% 
  filter(genes %in% adenomarkers)

# Tidy up dataframe for ggplot
adeno_cancerExpr <- adenomarker_impact %>% select(genes, b_cancer_T790M_neg, b_cancer_T790M_pos) %>% 
  gather(key="T790M", value = "Log2FPKM", b_cancer_T790M_neg, b_cancer_T790M_pos) %>% 
  mutate(T790M = case_when(
    T790M=="b_cancer_T790M_neg" ~ "T790M-",
    T790M=="b_cancer_T790M_pos" ~ "T790M+"
  ))
adeno_cancerSD <- adenomarker_impact %>% select(genes, se_cancer_T790M_neg, se_cancer_T790M_pos) %>% 
  gather(key="T790M", value = "SD", se_cancer_T790M_neg, se_cancer_T790M_pos) %>% 
  mutate(T790M = case_when(
    T790M=="se_cancer_T790M_neg" ~ "T790M-",
    T790M=="se_cancer_T790M_pos" ~ "T790M+"
  ))
adeno_toPlot <- inner_join(adeno_cancerExpr, adeno_cancerSD, by=c("genes", "T790M")) %>% 
  rename(Genes = "genes") %>% 
  mutate(ymin = Log2FPKM - SD,
         ymax = Log2FPKM + SD,
         ymin = if_else(ymin < 0, 0, ymin))
# Arrange T790M+
adeno_toPlot$T790M <- factor(adeno_toPlot$T790M, levels=c("T790M+", "T790M-"))
adeno_toPlot$Genes <- factor(adeno_toPlot$Genes, levels=c("NAPSA", "NKX2-1", "SFTA2", "SFTA3"))
# Get manual p-values
pvals <- adenomarker_impact[match(adenomarker_impact$genes, levels(adeno_toPlot$Genes)), 'c.pval', drop=TRUE]
pval_plot <- list()
pval_plot[["p.value"]] <- pvals

# Color for T790M
colorT790M <- c(anno_pal2[2], "darkgrey")

# Generate manual significance line since p-values are manually generated from TUMERIC
adeno_toPlot <- adeno_toPlot %>% 
  mutate(ylabel = 12.5)
df_line <- tibble(
  T790M = c("T790M-", "T790M-", "T790M+", "T790M+"),
  ypos = c(12, 12.35, 12.35, 12)
)
pval_df <- tibble(
  Genes = levels(adeno_toPlot$Genes),
  pval = paste("P = ", signif(pvals, 3), sep=""),
  x=1.5, y=13
)

plot_impact_adeno <- ggplot(adeno_toPlot, aes(x=T790M, y=Log2FPKM)) +
  # geom_bar(aes(fill=T790M, group=T790M), stat="identity", position="dodge") +
  geom_point(aes(color=T790M), 
             position=position_dodge(width = 0.3),
             size=3) +
  geom_errorbar(aes(ymin = ymin, 
                    ymax = ymax,
                    color=T790M), 
                position=position_dodge(width = 0.3),
                width=0.25, color="black", alpha=0.5) +
  geom_path(data=df_line, aes(x=T790M, y=ypos, group=1)) +
  geom_text(data=pval_df, aes(x=x, y=y-0.25, label=pval), size=4) +
  # scale_fill_manual(values=colorT790M)+
  # scale_color_manual(values=colorT790M) +
  ylim(c(0, 14)) +
  facet_wrap(~Genes, nrow=1) +
  labs(x="Genes", y="Log2(FPKM + 1)", title="Lung Adenocarcinoma Markers Expression in Cancer Cells") +
  theme(panel.grid.minor = element_blank(),
        strip.background = element_rect(colour="black", fill="white"),
        title = element_text(size=14, hjust=1),
        axis.title.x.bottom = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_text(size=16, hjust=0.5),
        legend.position = "bottom")

ggsave(paste0("adenoMarker_tumericPlot_IMPACT_", today_date, ".pdf"), plot_impact_adeno, width=8, height=5, useDingbats=FALSE)