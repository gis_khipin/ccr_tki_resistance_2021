#' 2021-6-18: Cleaned up code for CCR publication upload

library(tidyverse)
library(ReactomePA)
library(ggrepel)
source("../misc_codes/R_common_func.R")
source("../misc_codes/copy_number/copy_number_functions.R")

today_date <- Sys.Date()

theme_set(theme_bw(base_size = 18))

drivers <- load_drivers()

# Read TUMERIC deconvolution
pval_tumeric <- read_csv("../data_file/TUMERIC_results.csv")
pval_tumeric <- pval_tumeric %>% 
  mutate(c.pval = if_else(c.pval == 0, 10e-5, c.pval),
         s.pval = if_else(s.pval == 0, 10e-5, s.pval))
pval_tumeric$c.pval_adjusted <- p.adjust(pval_tumeric$c.pval, method="fdr")
pval_tumeric$s.pval_adjusted <- p.adjust(pval_tumeric$s.pval, method="fdr")

# Flip so that T790M+ is on the left
pval_tumeric$delta_c <- -pval_tumeric$delta_c

# Calculate where FDR is equal to 0.25
fdr_0.25 <- pval_tumeric %>%
  arrange(c.pval) %>% 
  filter(c.pval_adjusted <= 0.25) %>% 
  pull(c.pval) %>% 
  nth(n=-1L)
    
# 2019-2-19: Volcano plot. Lung cancer histo markers are hand-curated for annotation purpose
histo_markers <- read_tsv("Histo_Markers.tsv", col_names = c("genes", "types"))
adeno_markers <- histo_markers %>%
  filter(types=="Lung_Adenocarcinoma") %>% 
  pull(genes)
# Exclude TMC5 from plotting
adeno_markers <- tibble(genes = c("SFTA2", "SFTA3", "NAPSA", "NKX2-1")) %>% 
  mutate(type = "adeno_marker")

alveolar_marker <- tibble(genes = c("SFTPB", "SFTPC", "PDPN", "AGER")) %>% 
  mutate(type = "alveolar_marker")
  
# Pulmonary differentiation related genes (Manuscript extended data)
other_marker <- tibble(genes = c("KYNU", "SLC34A2", "C16orf89", "FOLR1", "LPCAT1",
                                 "KCNK5", "FBP1", "SMPDL3B", "METTL7B", "HNF1B", "AQP3",
                                 "SELENBP1", "HOPX")) %>% 
  mutate(type = "other")
# These needs to have FDR <= 0.05 to highlight
marker_withsig <- pval_tumeric %>% 
  filter(genes %in% other_marker$genes, c.pval_adjusted <= 0.25) %>% 
  pull(genes)


# Wnt signalling genes based on msigDB
msigdb <- qusage::read.gmt("msigdb.v6.2.symbols.gmt")
kegg_wnt <- msigdb[["KEGG_WNT_SIGNALING_PATHWAY"]]
wnt <- tibble(genes = pval_tumeric %>% 
  filter(genes %in% kegg_wnt) %>%
  pull(genes)) %>% 
  mutate(type = "Wnt")
# Genes on chromosome 3q23
threeq23 <- unique(unlist(msigdb[grepl("^chr3q23", names(msigdb))]))

# Mark only adeno plus ROS1 and KYNU
tomark <- bind_rows(adeno_markers,
                    tibble(genes=c("ROS1", marker_withsig), type="adeno_marker"))

# Highlight some genes from GISTIC amplification peak
gistic_t790m_neg <- read_tsv("gistic_amp_genes.txt") %>% 
  parse_peak_genes(drivers_list = drivers$all_drivers)
threeq23_genes <- gistic_t790m_neg %>% 
  filter(cytoband=="3q23") %>% 
  pull(all_genes_gistic) %>% 
  str_split(",") %>% 
  unlist()

toHighlight <- 
  # 2019-9-27: Only highlight specific genes
  filter(pval_tumeric, genes %in% tomark$genes) %>% 
  left_join(tomark, by="genes") %>% 
  replace_na(list("type" = "None")) %>% 
  mutate(alpha_text = if_else(grepl('Lung_Adenocarcinoma|other|marker|Wnt', type), -log10(min(c.pval_adjusted)), -log10(c.pval_adjusted)),
         size_text = if_else(grepl('Lung_Adenocarcinoma|other|marker|Wnt', type), -log10(min(c.pval_adjusted)), -log10(min(c.pval_adjusted))/2))

# Fig 2b volcano plot
ggplot(pval_tumeric, aes(y=-log10(c.pval), x=delta_c)) +
  scale_y_continuous(breaks = c(-log10(fdr_0.25), 5, 10, 15, 20),
               labels = c("FDR = 0.25", 5, 10, 15, 20)) +
  geom_point(aes(alpha=-log10(c.pval)), show.legend = FALSE) +
  geom_point(data = toHighlight,
             aes(y= -log10(c.pval), x=delta_c,
                 alpha=alpha_text, color=type), show.legend = FALSE) +
  geom_hline(yintercept = -log10(fdr_0.25), linetype=2) +
  geom_text_repel(data=toHighlight, 
                  aes(label=genes, color=type, 
                      alpha=alpha_text, size=size_text), 
                  show.legend = FALSE) +
  scale_color_manual(values=c("adeno_marker" = "red", "None" = "black", "other" = "blue",
                              "Wnt" = "purple", "alveolar_marker" = "dark green")) +
  scale_size(range = c(3, 5)) +
  labs(x = bquote("Log Fold Change in Cancer Cell Expression"),
       y = bquote(-log[10] * "(p-values)")) +
  xlim(c(-10, 10)) +
  ggsave(paste0("volcano_plot_TUMERIC_", today_date, ".pdf"), width=10, height=6, device=cairo_pdf)