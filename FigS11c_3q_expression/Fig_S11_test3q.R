#' 2021-6-18: Cleaned up code for CCR upload

source("../misc_codes/copy_number/copy_number_functions.R")
source("../misc_codes/R_common_func.R")
library(tidyverse)
library(ggrepel)
library(parallel)
library(doParallel)
library(foreach)
library(ggstatsplot)
# library(mygene)

today_date <- format(Sys.time(), "%Y-%m-%d")

# Read in TUMERIC results
tumeric <- read_csv("../data_file/TUMERIC_results.csv")

# HGNC gene mapping ID in RSEM pipeline
hgnc_info <- read_rds("RSEM_ID_mapped_HGNC_HG19.rds")

registerDoParallel(cores=6)
theme_set(theme_bw(base_size = 20))
cytoband <- load_cytoband(cytoband_ifile = "../misc_codes/references/cytoBand.txt")
# Find 3q location
cytoband3q <- cytoband$cytoband_coord %>% 
  filter(chromosome==3 & grepl("q", arm)) %>% 
  summarise(chromosome=unique(chromosome),
            Start=min(Start),
            End=max(End))

# Use msigDB to find 3q genes
msigdb <- qusage::read.gmt("msigdb.v6.2.symbols.gmt")
# 
gene_3q <- msigdb[names(msigdb)[grepl("chr3q", names(msigdb))]] %>% unlist %>% unique

# Get chromosomal location info
gene_3q_tx <- mygene::queryMany(gene_3q, scopes="symbol", species="human", fields=c("entrezgene", "symbol",
                                                                                    "genomic_pos_hg19.chr",
                                                                                    "genomic_pos_hg19.start",
                                                                                    "genomic_pos_hg19.end",
                                                                                    "pathway"))
# Handle chromosomal loc
chr_loc <- sapply(sapply(gene_3q_tx$genomic_pos_hg19, unlist), paste, collapse = ";")
# separate and keep only 2, 4 and 6 for those with multiple chrom loc
chr_loc <- lapply(chr_loc, function(x){
  bufstr <- strsplit(x[[1]], ";")
  bufstr <- bufstr[[1]]
  if(length(bufstr) > 3){
    finstr <- paste(bufstr[c(2,4,6)], collapse=";")
    return(finstr)
  } else{
    return(x)
  }
})

gene_3q_tx_df <- as_tibble(gene_3q_tx) %>% 
  mutate(genomic_pos_hg19 = unlist(chr_loc)) %>% 
  filter(!genomic_pos_hg19 == "") %>% 
  separate(genomic_pos_hg19, into=c("chr", "end_position", "start_position"), sep = ";", remove=FALSE)

# Load masterlist
selected_samples <- read_tsv("../data_file/samples_masterlist.tsv")
normalized_expr <- read_csv("../data_file/impact_expression_fpkm.csv")

# Only use single samples with RNA to test
samples_withRNA <- selected_samples %>% 
  filter(Cohort=="IMPACT", RNA_Seq == "Yes") %>% 
  pull(Tumor_Sample_Barcode)

# Only want interested features
samples_features <- selected_samples %>% 
  filter(Tumor_Sample_Barcode %in% samples_withRNA) %>% 
  dplyr::select(Tumor_Sample_Barcode, purity, ploidy, event_3q, Cohort, T790M) %>% 
  mutate(event_3q = if_else(event_3q=="Amp", 1, 0)) %>% 
  replace_na(list(event_3q = 0))

# Subset to samples with RNA
normalized_expr <- normalized_expr[, c("rsem_geneNames", samples_withRNA)] %>% 
  dplyr::rename(hgnc_symbol = "rsem_geneNames")

# Look at 3q expression across cohort
all_3q_ex <- normalized_expr %>% 
  filter(hgnc_symbol %in% gene_3q) %>% 
  gather(matches("A[0-9]+|GSK|BGI"), value=normalized_expression, key=Tumor_Sample_Barcode) %>% 
  inner_join(samples_features, by="Tumor_Sample_Barcode")

# Quick violin plot to see if expr is higher
ggbetweenstats(all_3q_ex, x=T790M, y=normalized_expression)
ggbetweenstats(all_3q_ex, x=event_3q, y=normalized_expression)

all_3q_ex_impact <- all_3q_ex %>% 
  filter(Cohort=="IMPACT")

# Try linear mixed effect model
library(lmerTest)
lm_all3q <- lmer(normalized_expression ~ T790M + purity + event_3q + (1|hgnc_symbol), data=all_3q_ex_impact)
# 2.5% and 97.5% confidence interval of 3q amplification is positive around 0.2 to 0.3
# implying a positive effect on expression
confint3q <- confint(lm_all3q)

# Conventional linear model with purity and T790M status as co-variate
length_toProcess <- length(gene_3q)
# Test all genes on 3q
all_stats = foreach(i=1:length_toProcess, .combine=bind_rows) %dopar% {
  gene = gene_3q[i]
  # Subset to only the gene, test only IMPACT cohort
  expr_df <- normalized_expr %>% 
    filter(hgnc_symbol == gene) %>% 
    gather(matches("A[0-9]+|GSK|BGI"), value=normalized_expression, key=Tumor_Sample_Barcode) %>% 
    inner_join(samples_features, by="Tumor_Sample_Barcode") %>% 
    filter(Cohort=="IMPACT")
  if(nrow(expr_df) > 0){
  # regression
    lm_gene <- lm(normalized_expression ~ T790M + purity + event_3q, expr_df)
    pval_gene <- summary(lm_gene)$coefficients["event_3q",]
    confint_gene <- confint(lm_gene)["event_3q",]
    stat_df <- t(as.data.frame(c(pval_gene, confint_gene))) %>% 
      as_tibble %>% 
      mutate(hgnc_symbol = gene)
    colnames(stat_df) <- c("estimate", "stdError", "tval", "pval", "confint_lb", "confint_ub", "hgnc_symbol")
    # Merge chr info
    stat_df <- stat_df %>% left_join(gene_3q_tx_df %>% select(query, chr, start_position, end_position), 
                                     by=c("hgnc_symbol" = "query"))
    # all_stats <- bind_rows(all_stats, stat_df)
  }
}
# Remove genes that is outside 3q (wrong annotation by msigDB?). Note that
# I updated msigDB a while after the supplementary figure is produced and
# this caused an additional gene to be added to the final dataframe. This
# affected the FDR value and may produce a slightly different plot. 
# However you can compare the plot and find that it's almost identical
# with the main conclusion being the same (3q amplification leads to 
# higher genes expression)
all_stats <- all_stats %>% 
  # Remember to convert to numeric!
  mutate(start_position = as.numeric(start_position),
         end_position = as.numeric(end_position)) %>% 
  # Keep those without annotation
  filter((start_position >= cytoband3q$Start & end_position <= cytoband3q$End) |
           is.na(start_position))

all_stats$padj <- p.adjust(all_stats$pval, method="BH")
all_stats <- all_stats %>% 
  arrange(padj)
write_tsv(all_stats, paste0("all_3q_genes_test_lm_purityT790MCovariate_", today_date, ".tsv"))
# Merge with tumeric
all_stats %>% 
  inner_join(tumeric, by=c("hgnc_symbol" = "genes")) %>% 
  write_tsv(paste0("tumeric_3q_test_lm_purityT790MCovariate_", today_date, ".tsv"))
all_stats_with_tumeric <- all_stats %>% 
  left_join(tumeric, by=c("hgnc_symbol" = "genes")) %>% 
  mutate(tumeric_lab = case_when(
    c.pval_adjusted <= 0.05 & s.pval_adjusted <= 0.05 ~ "CS",
    c.pval_adjusted <= 0.05 ~ "C",
    s.pval_adjusted <= 0.05 ~ "S",
    TRUE ~ "NS"
  ))
# Find FDR cutoff
fdr_cut <- all_stats_with_tumeric %>%
  filter(padj <= 0.05) %>% 
  arrange(desc(pval)) %>% 
  pull(pval) %>% 
  .[1]

# Volcano plot
ggplot(all_stats_with_tumeric, aes(x=estimate, y=-log10(pval),
                                          color = tumeric_lab)) +
  scale_color_manual(values=c("NS" = "black",
                              "C" = "red",
                              "S" = "blue")) +
  geom_point() +
  geom_hline(yintercept = -log10(fdr_cut), linetype=2) +
  geom_text_repel(data = all_stats_with_tumeric %>% filter(padj <= 0.05 |
                                                             c.pval_adjusted <= 0.05 |
                                                             s.pval_adjusted <= 0.05),
                  aes(x = estimate, y = -log10(pval), label=hgnc_symbol)) +
  scale_y_continuous(breaks = c(0, 2, -log10(fdr_cut), 4, 6),
                     labels = c("0", "2", "FDR 5%", "4", "6")) +
  labs(x = "LM Coefficient", y = "-Log10(pval)") +
  ggsave(paste0("3q_vs_no3q_volcano_", today_date, ".pdf"),
         width=12, height=7)

# Midpoint for plotting
all_stats <- all_stats %>% 
  mutate_at(c("start_position", "end_position"), as.numeric) %>% 
  mutate(midpoint = (start_position + end_position)/2)

# Plot these
ggplot(all_stats, aes(x=midpoint, y=estimate)) +
  geom_point(aes(alpha=-log10(padj), color=padj <= 0.1, size=-log10(padj))) +
  geom_errorbar(data=all_stats %>% filter(padj <= 0.1),
                aes(x=midpoint,
                    ymin=confint_lb, ymax=confint_ub),
                alpha=0.25) +
  geom_hline(yintercept = 0, linetype=2) +
  geom_text_repel(data=all_stats %>% filter(padj <= 0.1),
                  aes(x=midpoint, y=estimate, label=hgnc_symbol),
                  min.segment.length = 0, nudge_x = 5000000, alpha=1,
                  nudge_y = 1, segment.alpha = 0.25, segment.color = "blue") +
  labs(x="Chromosomal Location at Arm 3q", y="Linear Model Effect Size") +
  theme(axis.text.x = element_blank()) +
  ggsave(paste0("differences_3q_genes_chromLocation_", today_date, ".pdf"), width=16, height=8)

#' Figure S11c
ggplot(all_stats, aes(x=midpoint, y=estimate)) +
  geom_point(aes(alpha=-log10(padj), color=padj <= 0.1, size=-log10(padj))) +
  geom_errorbar(data=all_stats %>% filter(padj <= 0.1),
                aes(x=midpoint,
                    ymin=confint_lb, ymax=confint_ub),
                alpha=0.25) +
  geom_hline(yintercept = 0, linetype=2) +
  labs(x="Chromosomal Location at Arm 3q", y="Linear Model Effect Size") +
  theme(axis.text.x = element_blank()) +
  ggsave(paste0("differences_3q_genes_chromLocation_noLabel_", today_date, ".pdf"), width=10, height=8)


# Side note: The above test can be done for all genes ocross all chromosomes and you will find that only the 3q genes are elevated in the all-chromosomes plot