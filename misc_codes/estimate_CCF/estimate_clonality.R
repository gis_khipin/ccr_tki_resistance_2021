#!/bin/R-3.3.1
# Script adapted from Science paper's for EstimateClonality

args <- commandArgs(trailingOnly = T)  # only arguments is taken

samplelist <- args[1]
segdata <- args[2]
muttable <- args[3]
wd <- args[4]

# First of all load the needed packaged
suppressPackageStartupMessages(library(gdata))
suppressPackageStartupMessages(library(limma))
suppressPackageStartupMessages(library(sequenza))
suppressPackageStartupMessages(library(bootstrap))
suppressPackageStartupMessages(library(boot))
suppressPackageStartupMessages(library("EstimateClonality"))
suppressPackageStartupMessages(library("foreach"))
suppressPackageStartupMessages(library("doMC"))
suppressPackageStartupMessages(library("tidyr"))
suppressPackageStartupMessages(library("dplyr"))
registerDoMC(8)

samplelist <- read.delim(samplelist, header=F)

mutation.table <- read.delim(muttable, header = TRUE, stringsAsFactors = FALSE, sep = "\t",
  fill = TRUE)

seg.mat.copy <- read.table(segdata, header = TRUE, stringsAsFactors = FALSE, sep = "\t",
  fill = TRUE, quote = "'")

# combine copy number and mutation data ##### first of all, select only samples
# that are available in both seg.mat.copy and mutation.table
barcodes <- intersect(unique(seg.mat.copy$SampleID), unique(mutation.table[, 1]))

seg.mat.copy <- seg.mat.copy[seg.mat.copy$SampleID %in% barcodes, ]
# only keep seg.mat.copy of those samples available in both both seg.mat.copy and
# mutation.table
seg.mat.copy <- seg.mat.copy[seg.mat.copy$Chr %in% c(1:22), ]  # remove sex chromosomes
mutation.table <- mutation.table[mutation.table[, 1] %in% barcodes, ]
# only keep mutation.table of those samples available in both both seg.mat.copy
# and mutation.table
mutation.table <- mutation.table[mutation.table$Chr %in% c(1:22), ]  # remove sex chromosomes
mutation.table$Chr <- as.numeric(mutation.table$Chr)  # convert Chr from str to numeric values

#for (sampleid in samplelist$V1){
foreach(i=1:nrow(samplelist)) %dopar% {

################ One sample for eachtime #####################

TCGA.barcode = samplelist$V1[i] ################# patient identifier
print(paste0("Processing sample: ", TCGA.barcode))

###############################################################

# check if the patient info. stored in both mutation table and seg.mat.copy
if (!TCGA.barcode %in% barcodes) {
  stop("TCGA barcode not found in either mutation table or seg.mat.copy")
}

# select patient specific data and store his/ her data into sub.mat.copy and
# sub.mat.mut
sub.mat.copy <- seg.mat.copy[seg.mat.copy$SampleID == TCGA.barcode, , drop = FALSE]
sub.mat.mut <- mutation.table[mutation.table[, 1] == TCGA.barcode, , drop = FALSE]
sub.mat_forAnnotation <- sub.mat.mut[, c("Chr", "Start_position",
					 "End_position", "Hugo_Symbol",
					 "Variant_Classification",
					 "Protein_Change", "Reference",
					 "Alternate")]
colnames(sub.mat_forAnnotation) <- c("Chromosome", "Start_position", 
				       "End_position", "Hugo_Symbol",
				       "Variant_Classification", "Protein_Change",
				       "Reference_Base", "Alternate_Base")

# WHATS IS ChipNames +
# NormalFile???????????????????????????????????????????????????????????????????????????????
# check whether there is only one copy number profile
if (length(unique(as.character(sub.mat.copy$ChipNames))) > 1) {
  stop("You have more than one copy number profile")
}

if (length(unique(as.character(sub.mat.copy$NormalFile))) > 1) {
  stop("You have more than one normal copy number profile")
}

sub.mat.copy <- unique(sub.mat.copy)

# combine mutation and copy number
mut.table <- data.frame(t(sapply(1:nrow(sub.mat.mut), identify.mut.copy.number.ascat,
  sub.mat.mut, sub.mat.copy)), stringsAsFactors = FALSE)
mut.table <- mut.table[!is.na(mut.table$minor_cn), ]
mut.table <- mut.table[!is.na(mut.table$ref_counts), ]
mut.table <- mut.table[!duplicated(mut.table$mutation_id), ]
# mut.table includes columns: 'mutation_id', 'ref_counts', 'var_counts
# normal_cn', 'minor_cn major_cn', 'patient', 'Reference_Base', 'Alternate_Base'

### APPLY filters #### these 4 below are from input of clonality.estimation()
### function
sub.clonal.cut.off = 1
min.var.prop = 0.025
# min.alt.reads = 5
# min.depth = 30
min.alt.reads = 3
min.depth = 25

# minimum alternative reads cut off (only take those samples with alternative
# reads of at least 5)
mut.table <- mut.table[as.numeric(mut.table$var_counts) >= min.alt.reads, ]

# minimum coverage at site (only take those samples with sequencing depth of at
# least 30)
mut.table <- mut.table[as.numeric(mut.table$var_counts) + as.numeric(mut.table$ref_counts) >=
  min.depth, ]

# minimum variant proportion
mut.table <- mut.table[(as.numeric(mut.table$var_counts)/(as.numeric(mut.table$var_counts) +
  as.numeric(mut.table$ref_counts))) >= min.var.prop, ]

# calculating genome doubling estimates
sub.mat.copy.arm <- get.seg.mat.arm(sub.mat.copy)

# sub.mat.minor includes 'Sample', 'Chrom', 'Start', 'End', 'Num.probes' and
# 'val' (copy number of the minor allele in that sample) apply(X, MARGIN, FUN,
# ...): X - an array, including a matrix, MARGIN - a vector giving the subscripts
# which the function will be applied over. E.g., for a matrix 1 indicates rows, 2
# indicates columns, c(1, 2) indicates rows and columns

sub.mat.minor <- cbind(as.character(sub.mat.copy.arm$SampleID), sub.mat.copy.arm$Chr,
  sub.mat.copy.arm$Start, sub.mat.copy.arm$End, sub.mat.copy.arm$nProbes, apply(cbind(sub.mat.copy.arm$nA,
    sub.mat.copy.arm$nB), 1, min))
colnames(sub.mat.minor) <- c("Sample", "Chrom", "Start", "End", "Num.probes", "val")


# sub.mat.cn includes 'Sample', 'Chrom', 'Start, End, Num.probes, val (copy
# number = cn = nA+nB)
sub.mat.cn <- cbind(as.character(sub.mat.copy.arm$SampleID), sub.mat.copy.arm$Chr,
  sub.mat.copy.arm$Start, sub.mat.copy.arm$End, sub.mat.copy.arm$nProbes, sub.mat.copy.arm$cn)
colnames(sub.mat.cn) <- c("Sample", "Chrom", "Start", "End", "Num.probes", "val")

GD.pval <- genome.doub.sig(sample = TCGA.barcode, seg.mat.minor = sub.mat.minor,
   seg.mat.copy = sub.mat.cn, number.of.sim = 10000)
GD.status <- fun.GD.status(GD.pval = GD.pval, ploidy.val = round(sub.mat.copy$Ploidy[1]))
# GD.pval <- NA
# GD.status <- NA

TCGA.purity <- as.character(unique(sub.mat.copy[, grep("Purity", colnames(sub.mat.copy))]))

if (TCGA.purity > 1) {
  stop("\nYour tumour content is greater than 1! \nYou probably don't have an ASCAT estimate. \nThere is an alernative script for this scenario.")
  # use earlyORlate.strict for this scenario
}

# run earlyORlate function
TCGA.earlyLate <- earlyORlate(patient = TCGA.barcode, complete.mutation.table = mut.table,
  purity = TCGA.purity)

# combine outputs from earlyORlate and GD.pval, GD.status and TCGA.purity (add
# another 3 columns to TCGA.earlyLate)
TCGA.earlyLate <- cbind(TCGA.earlyLate, GD.pval, GD.status, TCGA.purity)

# Let's choose the columns of interest:
TCGA.earlyLate.out <- TCGA.earlyLate
TCGA.earlyLate.out$comb.timing <- NA  # add another column comb.timing and initialize it as NA

### Added 28-08-2017, to remove mutations with no CCF estimation ###
TCGA.earlyLate.out <- TCGA.earlyLate.out[!is.na(TCGA.earlyLate.out$absolute.ccf.0.95),
  ]

# determine values for comb.timing clonal and not occur after amplification or a
# genome-doubling event
TCGA.earlyLate.out[TCGA.earlyLate.out$absolute.ccf.0.95 >= sub.clonal.cut.off & !TCGA.earlyLate.out$timing %in%
  c("late"), "comb.timing"] <- "Early"
# subclonal and occur after amplification or a genome-doubling event
TCGA.earlyLate.out[TCGA.earlyLate.out$absolute.ccf.0.95 < sub.clonal.cut.off | TCGA.earlyLate.out$timing %in%
  c("late"), "comb.timing"] <- "Late"

TCGA.earlyLate.out <- separate(data=TCGA.earlyLate.out, col=mutation_id, into=c("Tumor_Sample_Barcode",
										"Chromosome", "Start_position",
										"Reference_Allele"), 
			       sep=':', remove=F)

# The stupid function has to force the type to be the same to join...
TCGA.earlyLate.out$Chromosome <- as.numeric(TCGA.earlyLate.out$Chromosome)
TCGA.earlyLate.out$Start_position <- as.numeric(TCGA.earlyLate.out$Start_position)
TCGA.earlyLate.out$Reference_Base <- as.character(TCGA.earlyLate.out$Reference_Base)
TCGA.earlyLate.out$Alternate_Base <- as.character(TCGA.earlyLate.out$Alternate_Base)

TCGA.earlyLate.out <- left_join(TCGA.earlyLate.out, sub.mat_forAnnotation,
				by=c("Chromosome", "Start_position",
				     "Reference_Base", "Alternate_Base"))

outfile_name = paste0(TCGA.barcode, "_earlylate.tsv")
outfile_path = file.path(wd, outfile_name)
df = as.matrix(TCGA.earlyLate.out)
write.table(df, file = outfile_path, quote = FALSE, sep = "\t", row.names = FALSE)
}
