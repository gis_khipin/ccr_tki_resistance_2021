library(tidyverse)

ref_seq_full <- read_tsv("ref_seq_hg19_ucsc_genomeBrowser_2018-9.bed",
                         col_types = cols("exonStarts" = col_character(),
                                          "exonEnds" = col_character()))

# Check if exon count is the same as after splitting exonStarts and exonEnds
# The end of the exon columns is actually empty after a comma, so
# the count is 1 more than the exonCount column. This should output nothing!
ref_seq_full %>% 
  select(exonCount, exonStarts, exonEnds) %>% 
  rowwise() %>% 
  mutate(countStart = length(unlist(str_split(exonStarts, ",")))) %>% 
  filter(exonCount != countStart - 1)

# Split into unique exons
ref_seq_exons_unique <- ref_seq_full %>% 
  select(name, chrom, exonStarts, exonEnds, name2) %>% 
  separate_rows(exonStarts, exonEnds, sep=",") %>% 
  # Remove the empty columns after comma for exonStarts column
  filter(exonStarts!="") %>% 
  mutate_at(c("exonStarts", "exonEnds"), as.numeric)

# Retain only unique region
ref_seq_exons_unique <- ref_seq_exons_unique %>% 
  group_by(chrom, exonStarts, exonEnds) %>% 
  filter(row_number() == 1) %>% 
  ungroup

# Sort and check for overlap
ref_seq_exons_unique %>% 
  group_by(chrom) %>% 
  arrange(chrom, exonStarts) %>% 
  filter((exonStarts - lag(exonEnds)) < 0)
