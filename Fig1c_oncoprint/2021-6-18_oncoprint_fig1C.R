#' 2021-6-18: Cleaned up code for CCR upload. Note that some annotations on the manuscript
#' are manually edited using Illustrator. This will get you 90% there with zero quantitative difference 

library(RColorBrewer)
library(circlize)
library(tidyverse)
library(ComplexHeatmap)
library(readxl)
library(colorspace)
library(viridis)
source("../misc_codes/R_common_func.R")

# Specify if naive should be included. Note that even if naive is included, 
# we only want to use them as baseline comparison, hence the genes plotted is the same as those in the IMPACT plot.
naive <- FALSE
filter <- TRUE
# today_date <- format(Sys.time(), "%Y-%m-%d")
today_date <- "2021-6-18"

drivers <- load_drivers()

inpmaf_file <- "../data_file/IMPACT_SNV_Indels_deleterious.maf"
inpmaf_gsk <- "../data_file/TreatmentNaive_SNV_Indels_deleterious.maf"
samples_masterlist <- "../data_file/samples_masterlist.tsv"

# GISTIC deletion and amplification
# gistic <- "~/TKI_Resistance/downstream/results_and_figures/masterlist/gistic_merge_snpArray_sequenza_amp_del_gene_per_sample.tsv"
# Use all sequenza
gistic_file <- "../data_file/GISTIC_results.tsv"

# Load chi-square test p-values
pval <- "../data_file/p_values_alterations.tsv"
pval <- read_tsv(pval)

# Plot p-values smaller than 0.4 (-log10(x) > 0.4), arbitrary to highlight certain genes!
# Plot those that are both significant and in the driver list
gene_to_plot <- pval %>% filter(p_val <= 0.1 & Hugo_Symbol %in% drivers$all_drivers)
# Add 3q amp as a "gene" to plot
gene_handpicked <- tibble(Genes="3q_Arm", Pathway="Others")
handpicked <- c("ERBB2", "PIK3CA", "PIK3CB", "EGFR", "PTEN", "MDM2", "RB1", #)
                # Wnt sgnalling from this paper: https://www.ncbi.nlm.nih.gov/pubmed/30635339
                "CTNNB1", "APC", "APC2", "AXIN1", "AXIN2")

genes_toPlot <- unique(c(gene_to_plot$Hugo_Symbol, gene_handpicked$Genes, handpicked))
# Remove CDKN2B as it's the same locus as CDKN2A and the CNV for these two genes are identical
genes_toPlot <- genes_toPlot[genes_toPlot!="CDKN2B"]

samples_masterlist <- read_tsv(samples_masterlist) %>%
  filter(!(Cohort=="TreatmentNaive" & is.na(EGFR_Prot_Change_NGS)))

# Convert HLA loss to yes and no in case we want to plot it
samples_masterlist <- samples_masterlist %>%
  # A047 is NOS. A450 is squamous, A465 is adenosquamous, A092 is small cell
  mutate(histo_biopsy = if_else(grepl("A450|A465|A092", Tumor_Sample_Barcode), "NonAdeno", "Adeno")) %>% 
  mutate(HLA_Loss = if_else(is.na(HLA_Loss), "No", "Yes"))

# Plot IMPACT only or with Naive
if(naive){
  samples_to_analyze <- unique(samples_masterlist$Tumor_Sample_Barcode)
} else{
  samples_masterlist <- samples_masterlist %>% filter(Cohort=="IMPACT")
  samples_to_analyze <- unique(samples_masterlist$Tumor_Sample_Barcode)
}

# Read the IMPACT MAF file.This MAF contains deleterious mutations. Please
# see main manuscript for definition of deleterious mutations
inpmaf <- read_tsv(inpmaf_file, col_types = cols(.default = "c"))
# Remove EGFR. Annotated as separate track
inpmaf <- inpmaf %>% 
  filter(Hugo_Symbol != "EGFR",
         # Manually removed PTEN mutations in the intron. Likely
         # not important.
         !(Hugo_Symbol == "PTEN" & Variant_Classification == "Intron"))

# Subset to useful column and merged indel and SNV to have the same "type"
inpmaf <- inpmaf %>% 
  select(Tumor_Sample_Barcode, Hugo_Symbol, Variant_Classification, HGVS_genomic_change,
         Protein_Change) %>%
  mutate(Alteration_Type = "SNV")

# Load naive cohort and process them the same way as the IMPACT ones
if(naive){
  inpmaf_naive <- read_tsv(inpmaf_gsk, col_types = cols(.default = "c")) %>%
    select(Tumor_Sample_Barcode, Hugo_Symbol, Variant_Classification, HGVS_genomic_change,
           Protein_Change) %>%
    mutate(Alteration_Type = "SNV") %>% 
    filter(Variant_Classification != "Intron") %>% 
    filter(Hugo_Symbol != "EGFR",
           !(Hugo_Symbol == "PTEN" & Variant_Classification == "Intron"))
  inpmaf <- bind_rows(inpmaf, inpmaf_naive)
  inpmaf <- inpmaf %>%
    filter(Tumor_Sample_Barcode %in% samples_to_analyze)
}

gistic <- read_tsv(gistic_file)
# Add types of events for fisher exact test
gistic <- gistic %>% mutate(Alteration_Type = "CNV")

# Add 3Q amplification as a somatic alteration
threeQ <- samples_masterlist %>% filter(event_3q == "Amp") %>% 
  select(Tumor_Sample_Barcode) %>% 
  mutate(Hugo_Symbol="3q_Arm",
         Variant_Classification="Amplification", Alteration_Type="CNV")

inpmaf <- bind_rows(inpmaf, gistic, threeQ)

# Combine MAF with samples masterlist to get the features and make annotation easier
# to read
inpmaf <- inpmaf %>% 
  inner_join(select(samples_masterlist, Tumor_Sample_Barcode, T790M, Cohort)) %>% 
  mutate(subgroup = case_when(Cohort=="TreatmentNaive" ~ "TN_T790M+",
                              T790M=="T790M+" ~ "TN_T790M+",
                              Cohort=="IMPACT" & T790M=="T790M-" ~ "T790M-"))

# Filter to the significant genes defined above
inpmaf_drivers <- filter(inpmaf, Hugo_Symbol %in% genes_toPlot) %>% unique
# If samples do not have any of the significant genes, still want to have
# a empty row instead of removing the samples from plot!
samples_nodriver <- setdiff(samples_to_analyze, inpmaf_drivers$Tumor_Sample_Barcode)
if (length(samples_nodriver) > 0){
  print(paste0("These samples has no alterations in important genes: ", paste(samples_nodriver, collapse = ",")))
  inpmaf_drivers <- bind_rows(inpmaf_drivers, tibble(Tumor_Sample_Barcode=samples_nodriver))
}

# Arrange by mutations for oncoprint. Ordering is based on visualization to determine
# the best way to arrange. We found arranging in the order of EGFR, TP53 and YEATS4
# yielded the best visualization
arrange_driver <- inpmaf_drivers %>% 
  filter(Hugo_Symbol %in% c("EGFR", "TP53", "YEATS4")) %>% 
  spread(key="Hugo_Symbol", value="Alteration_Type") %>% 
  select(Tumor_Sample_Barcode, EGFR, TP53, YEATS4)
samples_no_picked <- setdiff(samples_to_analyze, arrange_driver$Tumor_Sample_Barcode)
# For samples with no significant alterations in any selected genes, add empty row
arrange_driver <- bind_rows(arrange_driver, tibble(Tumor_Sample_Barcode=samples_no_picked))
# Collapse mutations as you can have multiple mutations in the same gene
arrange_driver <- arrange_driver %>% 
  group_by(Tumor_Sample_Barcode) %>% 
  summarise(EGFR = paste(EGFR[!is.na(EGFR)], collapse=""),
            TP53 = paste(TP53[!is.na(TP53)], collapse=""),
            YEATS4 = paste(YEATS4[!is.na(YEATS4)], collapse=""))

# Function to group samples into data frame with the values taken from variant_classification to
# prepare input for ComplexHeatmap
sort_MAF2df <- function(iMAF, signalling){
  iMAF <- inpmaf_drivers
  iMAF <- iMAF %>% select(Tumor_Sample_Barcode, Hugo_Symbol, Variant_Classification) %>% 
    group_by(Tumor_Sample_Barcode, Hugo_Symbol) %>% mutate(ind=row_number()) %>% 
    spread(Tumor_Sample_Barcode, Variant_Classification, fill=NA, drop=FALSE) %>% select(-ind)
  # Rename frame shift
  iMAF[iMAF=="Frame_Shift_Del"] <- "Frame_Shift_Indel"
  iMAF[iMAF=="Frame_Shift_Ins"] <- "Frame_Shift_Indel"
  iMAF[iMAF=="In_Frame_Ins"] <- "In_Frame_Indel"
  iMAF[iMAF=="In_Frame_Del"] <- "In_Frame_Indel"
  # Merge rows with same gene but different variant classification.
  oncodf_drivers <- summarise_all(iMAF, list(~paste0(na.omit(.), collapse=";")))
  # Remove elements with jsut semicolon so we can do rowsum properly
  # oncodf_drivers[oncodf_drivers==";" | oncodf_drivers==";;"] <- ""
  # Don't want to count LOH. Don't count first column (Symbol)
  oncodf_drivers <- mutate(oncodf_drivers, mutfreq=rowSums(!(oncodf_drivers[, 2:ncol(oncodf_drivers)] == "" | 
                                                               apply(oncodf_drivers[,2:ncol(oncodf_drivers)], c(1,2), 
                                                                     function(x) grepl("^LOH$|^Amp_LOH$|^Del_LOH$|^Hom_Del$|^Copy_Neutral_LOH$", x)))))
  oncodf_drivers <- oncodf_drivers %>% left_join(signalling, by="Hugo_Symbol")
  # Set those without pathway information to "others"
  oncodf_drivers <- oncodf_drivers %>% replace_na(list(Pathways="Others"))
  # Sort by pathways first, then rowsum. Pathways are manually set to highlight
  pathway_levels <- c("RTK/Ras","CellCycle", "NRF2", "Wnt","Hippo","MYC","Notch","PI3K","TGFb","Others")
  oncodf_drivers$Pathways <- factor(oncodf_drivers$Pathways, levels=pathway_levels)
  oncodf_drivers <- arrange(oncodf_drivers, Pathways, desc(mutfreq))
  return(oncodf_drivers)
}
# Rename pathways
drivers$signalling$Pathways[drivers$signalling$Pathways == "TP53"] <- "CellCycle"
drivers$signalling$Pathways[drivers$signalling$Pathways == "RTK_Ras"] <- "RTK/Ras"

# Generate dataframes using function defined above
oncodf_drivers <- sort_MAF2df(inpmaf_drivers, drivers$signalling)

# For drivers, remove less than 5% of cohort mutations
# Can filter to frequency if needed. if naive, definitely need to filter
# Also show set of special drivers handpicked to show
if(filter){
  oncodf_drivers <- oncodf_drivers %>% 
    filter(mutfreq/85 >= 0.05 | Hugo_Symbol %in% handpicked)
}
if(naive){
  oncodf_drivers <- oncodf_drivers %>% 
    filter(mutfreq/85 >= 0.05 | Hugo_Symbol %in% handpicked)
}

row_names_oncodf <- oncodf_drivers$Hugo_Symbol
pathway_names_oncodf <- oncodf_drivers$Pathways

# Get mutation frequency for sorting and labelling, 
# use only for the original plot without splitting into T790M- and T790M+
oncodf_mat <- as.matrix(select(oncodf_drivers, -Hugo_Symbol, -mutfreq, -Pathways))
rownames(oncodf_mat) <- row_names_oncodf

# Some samples has no mutations and no amplifications, so need to filter feature
feature_to_plot <- samples_masterlist %>% 
  left_join(arrange_driver, by="Tumor_Sample_Barcode")

# Manual arrange by EGFR mutation type
sgroup1 <- feature_to_plot %>% filter(T790M=="T790M+", Cohort=="IMPACT") %>% 
  arrange(Cohort, L858R, Exon19, desc(TP53), desc(YEATS4), desc(EGFR), desc(event_3q)) %>% 
  select(Tumor_Sample_Barcode) %>% unlist %>% as.character()
sgroup2 <- feature_to_plot %>% filter(T790M=="T790M-", Cohort=="IMPACT", is.na(T790M_earlyLate)) %>% 
  arrange(Cohort, L858R, Exon19, desc(TP53), desc(event_3q)) %>% 
  select(Tumor_Sample_Barcode) %>% unlist %>% as.character()
# Additional group if plotting naive
if(naive){
  sgroup3 <- feature_to_plot %>% filter(Cohort=="TreatmentNaive") %>% 
    arrange(Cohort, L858R, Exon19, desc(TP53), desc(event_3q)) %>% 
    select(Tumor_Sample_Barcode) %>% unlist %>% as.character()
}

if(naive){
  sample_order <- c(sgroup1, sgroup2, sgroup3)
  sample_order <- factor(sample_order, levels=sample_order)
  col_split <- cumsum(c(length(sgroup1), length(sgroup2), length(sgroup3)))
} else{
  sample_order <- c(sgroup1, sgroup2)
  col_split <- cumsum(c(length(sgroup1), length(sgroup2)))
}

oncodf_mat <- oncodf_mat[, as.character(sample_order)]

# Read in p-value to plot
pval_toPlot <- pval[match(rownames(oncodf_mat), pval$Hugo_Symbol),] %>% 
  select(Hugo_Symbol, p_val) %>% mutate(p_val_log10=-log10(p_val))

# Log the p-values to contrast better
pval_toPlot$p_val_log10 <- round(pval_toPlot$p_val_log10, 2)

# Get important features
feature_to_plot <- feature_to_plot %>% 
  select(Tumor_Sample_Barcode, T790M, Cohort, Gender, GII, histo_biopsy,
         TTP, tmb_coding, subtype_wilkerson,
         T790M_earlyLate, pLM_coding, Smoker, Stage, 
         GD.status, SNP_Array, T790M_CCF, chemo_days_beforeBiopsy, time_PD_to_biopsy,
         HLA_Loss, L858R, Exon19, OtherEGFR,
         Exon19_CCF, L858R_CCF, T790M_CCF, NMF_MutSig) %>% 
  rename(Coding_Mutations="tmb_coding") %>% 
  arrange(factor(Tumor_Sample_Barcode, levels=sample_order))

# Colors and shapes adapted from Jianbin.
alter_fun = list(
  background = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h-unit(0.5, "mm"), gp = gpar(fill = "#f0f0f0", col = "white", lwd=0.25))
  },
  Silent = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h-unit(0.5, "mm"), gp = gpar(fill = "#989898", col = "white", lwd=0.25))
  },
  Missense_Mutation = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h-unit(0.5, "mm"), gp = gpar(fill = colonco[2], col = "white", lwd=0.25))
  },
  Nonsense_Mutation = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h-unit(0.5, "mm"), gp = gpar(fill = colonco[4], col = "white", lwd=0.25))
  },
  Splice_Site = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h*0.33, gp = gpar(fill = colonco[1], col = "white", lwd=0.25))
  },
  Frame_Shift_Indel = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h*0.33, gp = gpar(fill = colonco[5], col = "white", lwd=0.25))
  },
  In_Frame_Indel = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h*0.33, gp = gpar(fill = colonco[3], col = "white", lwd=0.25))
  },
  Silent = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h*0.33, gp = gpar(fill = colonco[6], col = "white", lwd=0.25))
  },
  Translation_Start_Site = function(x, y, w, h) {
    grid.rect(x, y, w-unit(0.5, "mm"), h*0.33, gp = gpar(fill = colonco[8], col = "white", lwd=0.25))
  },
  Amplification = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(0.5, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(0.5, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y, "mm")
    grid.polygon(x=unit.c(left, midpoint, right),
                 y=unit.c(bottom, top, bottom),
                 gp = gpar(fill = "#fdae61", col = "#fdae61"))
  },
  Deletion = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(0.5, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(0.5, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y, "mm")
    grid.polygon(x=unit.c(left, midpoint, right),
                 y=unit.c(top, bottom, top),
                 gp = gpar(fill = "#74add1", col = "#74add1",alpha=1))
  },
  Hom_Del = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(1, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(1, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y+h, "mm")
    grid.circle(x=left, y=top,
                gp = gpar(fill = "#2166ac", col =  "#2166ac",alpha=1), 
                r=unit(0.5, "mm"))
  },
  Amp_LOH = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(1, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(1, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y+h, "mm")
    grid.circle(x=right, y=top,
                gp = gpar(fill = "#ef8a62", col =  "#ef8a62",alpha=1), 
                r=unit(0.3, "mm"))
  },
  Del_LOH = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(1, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(1, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y+h, "mm")
    grid.circle(x=left, y=top,
                gp = gpar(fill = "#67a9cf", col =  "#67a9cf",alpha=1), 
                r=unit(0.3, "mm"))
  },
  Copy_Neutral_LOH = function(x, y, w, h) {
    w = convertWidth(w*0.5 - unit(1, "mm"), "mm") 
    h = convertHeight(h*0.5-unit(1, "mm"), "mm") 
    left = convertX(x-w, "mm") 
    right = convertX(x+w, "mm") 
    midpoint = convertX(x, "mm")
    bottom = convertY(y-h, "mm") 
    top = convertY(y+h, "mm")
    grid.circle(x=midpoint, y=top,
                gp = gpar(fill = "#999999", col =  "#999999",alpha=1), 
                r=unit(0.3, "mm"))
  }
)

colonco=brewer.pal(9,"Set1")
col = c("Missense_Mutation" = colonco[2], "Nonsense_Mutation" = colonco[4],
        "Nonstop_Mutation"=colonco[6],"Stop_Codon_Del"=colonco[7],
        "Splice_Site"=colonco[1], "Frame_Shift_Indel"=colonco[5],
        "In_Frame_Indel"=colonco[3],"Translation_Start_Site"=colonco[8], 
        "Silent"="#989898", "Amplification"="#fdae61", "Deletion"="#74add1",
        "Copy_Neutral_LOH" = "#999999", "Amp_LOH" = "#ef8a62", "Del_LOH" = "#67a9cf",
        "Hom_Del" = "#2166ac")

# Construct annotation
bottom_annot <- data.frame(
  Subtypes = feature_to_plot$subtype_wilkerson,
  Smoker = feature_to_plot$Smoker,
  Gender = feature_to_plot$Gender,
  GD.status = feature_to_plot$GD.status,
  HistoBiopsy = feature_to_plot$histo_biopsy
)
rownames(bottom_annot) <- feature_to_plot$Tumor_Sample_Barcode


min_TTP <- min(feature_to_plot$TTP, na.rm = TRUE)
max_TTP <- max(feature_to_plot$TTP, na.rm = TRUE)
median_TTP <- median(feature_to_plot$TTP, na.rm = TRUE)

min_chemo <- min(feature_to_plot$chemo_days_beforeBiopsy, na.rm = TRUE)
median_chemo <- median(feature_to_plot$chemo_days_beforeBiopsy, na.rm = TRUE)
max_chemo <- max(feature_to_plot$chemo_days_beforeBiopsy, na.rm = TRUE)

min_pdBiopsy <- min(feature_to_plot$time_PD_to_biopsy, na.rm = TRUE)
median_pdBiopsy <- median(feature_to_plot$time_PD_to_biopsy, na.rm = TRUE)
max_pdBiopsy <- max(feature_to_plot$time_PD_to_biopsy, na.rm = TRUE)

# Generate color palette for bottom annotation
anno_pal <- brewer.pal(4, name="Dark2")
anno_pal10 <- viridis(10)
anno_pal2 <- c('#f2f0f7','#6a51a3')
anno_pal4 <- c('#f2f0f7','#cbc9e2','#9e9ac8','#6a51a3')
anno_col <- list(TTP = colorRamp2(c(min_TTP, median_TTP, max_TTP), viridis(3)),
  T790M_CCF = colorRamp2(seq(0,1,length.out = 20), c("grey", viridis(19))),
  Smoker = c("Yes" = anno_pal2[2], "No" = anno_pal2[1]),
  Subtypes = c("TRU" = c25[8], "PI" = c25[9], "PP" = c25[10]),
  GII = colorRamp2(seq(0,1,length.out = 20), viridis(20)),
  Gender = c("Male" = c25[19], "Female" = c25[15]),
  HistoBiopsy = c("NonAdeno" = anno_pal2[2], "Adeno" = anno_pal2[1]),
  SNP_Array = c("Yes" = anno_pal2[2], "No" = anno_pal2[1]),
  chemo_duration = colorRamp2(c(min_chemo, median_chemo, max_chemo), viridis(3)),
  time_PD_to_biopsy = colorRamp2(c(min_pdBiopsy, median_pdBiopsy, max_pdBiopsy), viridis(3)),
  GD.status = c("GD" = anno_pal2[2], "nGD" = anno_pal2[1]),
  HLA_Loss = c("Yes" = anno_pal2[2], "No" = anno_pal2[1]),
  OtherEGFR = c("Yes" = anno_pal2[2], "No" = anno_pal2[1]),
  T790M = colorRamp2(seq(0,1,length.out = 20), c("grey", viridis(19))),
  L858R = colorRamp2(seq(0,1,length.out = 20),  c("grey", viridis(19))),
  Exon19 = colorRamp2(seq(0,1,length.out = 20),  c("grey", viridis(19))),
  Signature = c("Smoking" = c25[15], "Aging" = c25[14],
                "AgingS3" = c25[1], "APOBEC" = c25[10],
                "AgingS16" = c25[8]))

pval_annot <- data.frame(`p-values` = pval_toPlot$p_val_log10, check.names = FALSE) %>% as.matrix()
rownames(pval_annot) <- format(round(pval_toPlot$p_val, digits = 2), digits = 2)

# Scale mutations using square root
logged_burden <- data.frame(Coding_Mutations=sqrt(feature_to_plot$Coding_Mutations),
                            unscaled_label=feature_to_plot$Coding_Mutations)
max_burden <- max(logged_burden[, "Coding_Mutations", drop=FALSE])
rownames(logged_burden) <- feature_to_plot$Tumor_Sample_Barcode

# Barplot with true mutation burden
ta_f <- anno_points(logged_burden[, "Coding_Mutations", drop=FALSE], which=c("column"),
                    gp=gpar(fill=c("#9e9ac8")), axis = FALSE, ylim=c(0, ceiling(max_burden)))

# Mutational signature dataframe saved from Mutational Pattern
mut_sig <- read_rds("all_fitSig.rds")
mut_prop <- t(t(mut_sig$contribution) / colSums(mut_sig$contribution))
mut_prop <- as_tibble(t(mut_prop), rownames="Tumor_Sample_Barcode")
mut_prop <- mut_prop %>% 
  mutate(Aging = Signature.1,
         APOBEC = Signature.2 + Signature.13,
         Smoking = Signature.4,
         DNA_DSBR = Signature.3,
         DNA_MMR = Signature.6,
         Others = 1 - APOBEC - Aging - Smoking - DNA_DSBR - DNA_MMR) %>% 
  select(-matches("Signature\\."))
mut_prop_df <- as.data.frame(mut_prop %>% select(-Tumor_Sample_Barcode))
rownames(mut_prop_df) <- mut_prop$Tumor_Sample_Barcode
# Reorder
mut_prop_df <- mut_prop_df[feature_to_plot$Tumor_Sample_Barcode, ]
mut_color <- c(c25[7], c25[3], c25[25], c25[19], c25[20], c25[13])
ta_sig <- anno_barplot(mut_prop_df, which="column",
                       gp = gpar(fill = mut_color,
                                 col = mut_color),
                       axis=FALSE, baseline=0)
# Construct legend for signature
sig_leg <- Legend(
  labels = c("Aging", "APOBEC", "Smoking", "DNA_DSBR", "DNA_MMR", "Others"),
  title="Signature", type="grid", 
  legend_gp=gpar(fill = mut_color,
                 col = mut_color),
  title_gp = gpar(fontsize = 12),
  labels_gp = gpar(fontsize = 14)
)

# Top annotation
ta_df <- data.frame(
  HistoBiopsy = feature_to_plot$histo_biopsy,
  Smoker = feature_to_plot$Smoker,
  Gender = feature_to_plot$Gender,
  T790M = feature_to_plot$T790M_CCF,
  L858R = feature_to_plot$L858R_CCF,
  Exon19 = feature_to_plot$Exon19_CCF,
  OtherEGFR = feature_to_plot$OtherEGFR,
  GD.status = feature_to_plot$GD.status,
  Subtypes = feature_to_plot$subtype_wilkerson 
)
rownames(ta_df) <- feature_to_plot$Tumor_Sample_Barcode

# Change sample name to just patient name and remove the "IM" annotation
patientNames <- gsub("(A[0-9]+).*", "\\1", rownames(bottom_annot))
ba <- HeatmapAnnotation(df = bottom_annot,
                        sampleNames = anno_text(patientNames),
                        name = "feature",
                        col = anno_col,
                        show_annotation_name = T,
                        annotation_name_side = "left",
                        gap=unit(0.5, "mm"),
                        which="column",
                        annotation_legend_param = list(title_gp = gpar(fontsize = 12),
                                                       labels_gp = gpar(fontsize = 14)),
                        gp=gpar(col="white", lwd=2))
# Annotation without lavelling
ba_nolabel <- HeatmapAnnotation(df = bottom_annot,
                                sampleNames = anno_text(patientNames),
                                name = "feature_nolabel",
                                col = anno_col,
                                show_annotation_name = FALSE,
                                gap=unit(0.5, "mm"),
                                which="column",
                                annotation_legend_param = list(title_gp = gpar(fontsize = 12),
                                                               labels_gp = gpar(fontsize = 14)),
                                gp=gpar(col="white", lwd=2))

ta <- HeatmapAnnotation(TMB = ta_f, 
                        Signature = ta_sig,
                        df = ta_df,
                        col=anno_col,
                        name="top_annotation",
                        which="column",
                        show_annotation_name = TRUE,
                        annotation_name_side = "left",
                        height = unit(2.8, "in"), annotation_height = c(3,3,1,1,1,1,1,1,1,1,1),
                        gap=unit(0.025, "in"),
                        annotation_legend_param = list(title_gp = gpar(fontsize = 12),
                                                       labels_gp = gpar(fontsize = 14)),
                        gp=gpar(col="white", lwd=2),
                        show_legend = TRUE)

# No labelling as we only need to label either one of T790M+ or T790M- track
ta_nolabel <- HeatmapAnnotation(TMB_nolabel = ta_f, 
                                Signature_nolabel = ta_sig,
                                df=ta_df,
                                col=anno_col,
                                name="top_annotation_nolabel",
                                which="column",
                                show_annotation_name = FALSE,
                                height = unit(2.8, "in"), annotation_height = c(3,3,1,1,1,1,1,1,1,1,1),
                                gap=unit(0.025, "in"),
                                annotation_legend_param = list(title_gp = gpar(fontsize = 12),
                                                               labels_gp = gpar(fontsize = 14)),
                                gp=gpar(col="white", lwd=2),
                                # show_legend = c(TRUE, FALSE, FALSE, FALSE))
                                show_legend = TRUE)

# Column splitting on T790M stat
if(naive){
  feature_to_plot <- feature_to_plot %>% 
    mutate(T790M = if_else(Cohort=="TreatmentNaive", "TreatmentNaive", T790M))
  col_split_t790m <-  feature_to_plot$T790M[match(colnames(oncodf_mat), feature_to_plot$Tumor_Sample_Barcode)]
} else{
  col_split_t790m <- feature_to_plot$T790M[match(colnames(oncodf_mat), feature_to_plot$Tumor_Sample_Barcode)]
}

# 2019-4-2: Split into T790M- and T790M+ heatmap separately
t790m_pos_oncodf_mat <- oncodf_mat[, col_split_t790m=="T790M+"]
t790m_neg_oncodf_mat <- oncodf_mat[, col_split_t790m=="T790M-"]
if(naive){
  naive_oncodf_mat <- oncodf_mat[, col_split_t790m=="TreatmentNaive"]
}
#' 2019-4-2: Function to calculate row alteration frequency excluding LOH. For annotation purpose
get_mutFreq <- function(onco_mat){
  mutfreq=rowSums(!(onco_mat == "" | 
                      apply(onco_mat, c(1,2), 
                            function(x) grepl("^LOH$|^Amp_LOH$|^Del_LOH$|^Hom_Del$|^Copy_Neutral_LOH$", x))))
  mutfreq <- mutfreq / ncol(onco_mat)
  mutfreq <- paste(round(mutfreq * 100), "%", sep="")
  mutfreq_df <- data.frame(
    mutationFreq = mutfreq
  )
  rownames(mutfreq_df) <- rownames(onco_mat)
  return(mutfreq_df)
}

t790m_pos_mutfreq <- get_mutFreq(t790m_pos_oncodf_mat)
t790m_neg_mutfreq <- get_mutFreq(t790m_neg_oncodf_mat)
if(naive){
  naive_mutfreq <- get_mutFreq(naive_oncodf_mat)
}

# Annotate significance on p-values
pval_annot[is.na(pval_annot)] <- -log10(0.0004)
pval_df <- as.data.frame(pval_annot)
pval_df[, "significance"] <- ""
pval_df[pval_df$`p-values` > -log10(0.1), "significance"] <- "*"
pval_df[pval_df$`p-values` > -log10(0.05), "significance"] <- "**"

# Annotate star for significance
pvalAnnot <- Heatmap(name = "pval", as.matrix(pval_df[, 1, drop=FALSE]), cluster_rows = FALSE, 
                     cluster_columns = FALSE,
                     cell_fun = function(j, i, x, y, width, height, fill) {
                       grid.text(sprintf("%s", pval_df[i, 2]), x, y, gp = gpar(fontsize = 10))
                       grid.rect(x = x, y = y, width = width, height = height, 
                                 gp = gpar(col = "white", fill = NA, lwd=4))
                     }, 
                     col=circlize::colorRamp2(seq(0,1.30,length.out = 20), viridis(20)),
                     split=pathway_names_oncodf, row_title = " ", 
                     show_row_names = FALSE, show_column_names = FALSE, show_heatmap_legend = TRUE)

ra_t790m_pos <- HeatmapAnnotation(frequency = anno_text(t790m_pos_mutfreq$mutationFreq, which="row"),
                                  which = "row", show_annotation_name = FALSE,
                                  annotation_legend_param =  list(title_gp = gpar(fontsize = 12),
                                                                  labels_gp = gpar(fontsize = 14)),
                                  border=TRUE)

ra_t790m_neg <- HeatmapAnnotation(frequency = anno_text(t790m_neg_mutfreq$mutationFreq, which="row"),
                                  which = "row", show_annotation_name = TRUE,
                                  annotation_legend_param =  list(title_gp = gpar(fontsize = 12),
                                                                  labels_gp = gpar(fontsize = 14)),
                                  border=TRUE)

oncoplot_pos <- oncoPrint(t790m_pos_oncodf_mat, get_type = function(x) strsplit(x, ";")[[1]],
                          alter_fun = alter_fun, col = col,
                          column_order = colnames(t790m_pos_oncodf_mat),
                          row_order = NULL,
                          right_annotation = ra_t790m_pos,
                          top_annotation = ta_nolabel[1:ncol(t790m_pos_oncodf_mat),],
                          bottom_annotation = ba_nolabel[1:ncol(t790m_pos_oncodf_mat), 6],
                          show_row_names = TRUE,
                          row_names_side = "left",
                          left_annotation = NULL,
                          show_pct = FALSE,
                          show_column_names = FALSE,
                          column_title="T790M+",
                          row_split=pathway_names_oncodf, gap=unit(5, "mm"),
                          name="oncoplot_pos", heatmap_legend_param = list(title="Alterations",
                                                                           title_gp = gpar(fontsize = 12),
                                                                           labels_gp = gpar(fontsize = 14)),
                          column_names_gp = gpar(fontsize=12),
                          show_heatmap_legend=FALSE, border=FALSE) #%v% ba[1:ncol(t790m_pos_oncodf_mat),]

oncoplot_neg <- oncoPrint(t790m_neg_oncodf_mat, get_type = function(x) strsplit(x, ";")[[1]],
                          alter_fun = alter_fun, col = col,
                          column_order = colnames(t790m_neg_oncodf_mat),
                          row_order = NULL,
                          left_annotation = ra_t790m_neg,
                          top_annotation = ta[39:59,],
                          right_annotation = NULL,
                          bottom_annotation = ba[39:59, 6],
                          show_pct = FALSE,
                          show_column_names = FALSE,
                          column_title="T790M-",
                          show_row_names = FALSE,
                          row_split=pathway_names_oncodf, gap=unit(5, "mm"),
                          name="oncoplot_neg", heatmap_legend_param = list(title="Alterations",
                                                                           title_gp = gpar(fontsize = 12),
                                                                           labels_gp = gpar(fontsize = 14)),
                          column_names_gp = gpar(fontsize=12), border=FALSE) #%v% ba[39:59,]

if(naive){
  oncoplot_naive <- oncoPrint(naive_oncodf_mat, get_type = function(x) strsplit(x, ";")[[1]],
                            alter_fun = alter_fun, col = col,
                            column_order = colnames(naive_oncodf_mat),
                            row_order = NULL,
                            left_annotation = NULL,
                            top_annotation = ta_nolabel[60:97,],
                            right_annotation = NULL,
                            bottom_annotation = ba_nolabel[60:97, 6],
                            show_pct = TRUE,
                            show_column_names = FALSE,
                            column_title="TreatmentNaive",
                            show_row_names = FALSE,
                            row_split=pathway_names_oncodf, gap=unit(5, "mm"),
                            name="oncoplot_naive", heatmap_legend_param = list(title="Alterations",
                                                                             title_gp = gpar(fontsize = 12),
                                                                             labels_gp = gpar(fontsize = 14)),
                            column_names_gp = gpar(fontsize=12), border=FALSE) 
}


label_breaks <- c(0, 3, 10)
tmb_label <- tibble(labelNames = label_breaks,
                    labelbreaks = sqrt(label_breaks))
# Draw line at 3Mb-1
lines_draw_at <- tmb_label[tmb_label$labelNames==10,]$labelbreaks

if(filter){
  today_date <- paste0(today_date, "_filterFrequency")
}

{
  if(naive){
    ofileName <- paste0("IMPACT_GSK_T790M_oncoprint_", today_date, ".pdf")
    width=23
    height=10
  } else{
    ofileName <- paste0("IMPACT_T790M_oncoprint_", today_date, ".pdf")
    width=18
    height=9
  }
  pdf(ofileName, width=width, height=height, useDingbats = FALSE)
  if(naive){
    draw(oncoplot_pos + pvalAnnot + oncoplot_neg + oncoplot_naive, # annotation_legend_list = list(ta_legend), 
         heatmap_legend_side = c("right"), 
         annotation_legend_side=c("bottom"), merge_legend=TRUE,
         annotation_legend_list = list(sig_leg))
  } else{
    draw(oncoplot_pos + pvalAnnot + oncoplot_neg, # annotation_legend_list = list(ta_legend), 
         heatmap_legend_side = c("right"), 
         annotation_legend_side=c("bottom"), merge_legend=TRUE,
         annotation_legend_list = list(sig_leg))
  }

  if(naive){
    numCohort = 3
  } else{
    numCohort = 2
  }
  
  # To also decorate second tmb barplot
  decorate_annotation("TMB",{
    # grid.yaxis(at = tmb_label$labelbreaks, label = tmb_label$labelNames)
    grid.lines(x = unit(c(0, 1), "npc"),
               y = unit(c(lines_draw_at, lines_draw_at), "native"),
               gp = gpar(col = "black", lty=2))
  })
  decorate_annotation("TMB_nolabel", {
    grid.yaxis(at = tmb_label$labelbreaks, label = tmb_label$labelNames)
    grid.lines(x = unit(c(0, 1), "npc"),
               y = unit(c(lines_draw_at, lines_draw_at), "native"),
               gp = gpar(col = "black", lty=2))
  })
  if(naive){
    decorate_annotation("TMB_nolabel", {
      grid.yaxis(at = tmb_label$labelbreaks, label = tmb_label$labelNames)
      grid.lines(x = unit(c(0, 1), "npc"),
                 y = unit(c(lines_draw_at, lines_draw_at), "native"),
                 gp = gpar(col = "black", lty=2))
    })
  }
  dev.off()
}
