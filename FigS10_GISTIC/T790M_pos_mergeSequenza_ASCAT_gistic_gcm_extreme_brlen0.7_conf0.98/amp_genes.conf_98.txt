cytoband	14q21.1	12q15	8q24.21	7p11.2	8q23.2	
q value	8.5962e-06	0.00085014	0.024183	0.071422	0.020161	
residual q value	8.5962e-06	0.00085014	0.055075	0.071422	1	
wide peak boundaries	chr14:36758410-38111869	chr12:68954781-69678873	chr8:96040144-146364022	chr7:55001510-56088000	chr8:1-146364022	
genes in wide peak	FOXA1	hsa-mir-1279	hsa-mir-1234	EGFR	hsa-mir-1234	
	PAX9	CPM	hsa-mir-939	GBAS	hsa-mir-939	
	NKX2-1	MDM2	hsa-mir-661	PSPH	hsa-mir-661	
	NKX2-8	RAP1B	hsa-mir-937	MRPS17	hsa-mir-937	
	MBIP	CPSF6	hsa-mir-1302-7	LANCL2	hsa-mir-1302-7	
	SLC25A21	SLC35E3	hsa-mir-151	VOPP1	hsa-mir-151	
	MIPOL1	NUP107	hsa-mir-30d	SEPT14	hsa-mir-30d	
	SFTA3	MIR1279	hsa-mir-1208	ZNF713	hsa-mir-1208	
	LOC100129794	SNORA70G	hsa-mir-1207	FKBP9L	hsa-mir-1207	
	MIR4503	LOC100507250	hsa-mir-1205		hsa-mir-1205	
			hsa-mir-1204		hsa-mir-1204	
			hsa-mir-548d-1		hsa-mir-548d-1	
			hsa-mir-2053		hsa-mir-2053	
			hsa-mir-548a-3		hsa-mir-548a-3	
			hsa-mir-3151		hsa-mir-3151	
			hsa-mir-1273		hsa-mir-1273	
			hsa-mir-875		hsa-mir-875	
			hsa-mir-3150		hsa-mir-3150	
			ADCY8		hsa-mir-3149	
			ANGPT1		hsa-mir-2052	
			ANXA13		hsa-mir-124-2	
			ATP6V1C1		hsa-mir-486	
			BAI1		hsa-mir-3148	
			COX6C		hsa-mir-4288	
			CYC1		hsa-mir-4287	
			CYP11B1		hsa-mir-548h-4	
			CYP11B2		hsa-mir-320a	
			DPYS		hsa-mir-548v	
			EEF1D		hsa-mir-383	
			EXT1		hsa-mir-598	
			GLI4		hsa-mir-1322	
			GML		hsa-mir-4286	
			GPR20		hsa-mir-124-1	
			GPT		hsa-mir-597	
			GRINA		hsa-mir-548i-3	
			HAS2		hsa-mir-596	
			HSF1		NAT1	
			EIF3E		NAT2	
			KCNQ3		ADCY8	
			KCNS2		ADRA1A	
			LY6E		ADRB3	
			LY6H		ANGPT1	
			MATN2		ANGPT2	
			MYC		ANK1	
			NDUFB9		ANXA13	
			TONSL		ASAH1	
			NOV		ASPH	
			ODF1		ATP6V1B2	
			TNFRSF11B		ATP6V1C1	
			ENPP2		BAI1	
			PLEC		BLK	
			POLR2K		BMP1	
			POU5F1B		POLR3D	
			PTK2		BNIP3L	
			PVT1		OSGIN2	
			RAD21		CA1	
			RPL8		CA2	
			RPL30		CA3	
			SDC2		CA8	
			ST3GAL1		CALB1	
			SLA		RUNX1T1	
			SNTB1		CDH17	
			SPAG1		CEBPD	
			SQLE		CHRNA2	
			STK3		CHRNB3	
			TAF2		CLU	
			TG		COX6C	
			KLF10		CRH	
			TRHR		CTSB	
			TRPS1		CYC1	
			TSTA3		CYP7A1	
			COL14A1		CYP11B1	
			UQCRB		CYP11B2	
			YWHAZ		ADAM3A	
			ZNF7		DECR1	
			ZNF16		DEFA1	
			PSCA		DEFA3	
			FZD6		DEFA4	
			LY6D		DEFA5	
			JRK		DEFA6	
			EIF3H		DEFB1	
			DGAT1		DEFB4A	
			GPAA1		DPYS	
			WISP1		DPYSL2	
			FOXH1		DUSP4	
			EBAG9		E2F5	
			RECQL4		EEF1D	
			LRRC14		EGR3	
			TTC35		EIF4EBP1	
			RIMS2		EPB49	
			MTSS1		EPHX2	
			PTDSS1		CLN8	
			ZNF623		EXT1	
			KIAA0196		EXTL3	
			HHLA1		EYA1	
			TRIB1		FABP4	
			HRSP12		FABP5	
			NDRG1		PTK2B	
			PGCP		FDFT1	
			COLEC10		FGFR1	
			KHDRBS3		FGL1	
			POP1		FNTA	
			PTP4A3		ADAM2	
			RNF139		GATA4	
			ZHX1		GEM	
			PUF60		GFRA2	
			ZHX2		GLI4	
			DENND3		GML	
			ZC3H3		GNRH1	
			EFR3A		NPBWR1	
			ARC		GPR20	
			BOP1		GPT	
			ZFPM2		GRINA	
			SCRIB		GSR	
			LRRC6		GTF2E2	
			DCAF13		HAS2	
			RNF19A		NRG1	
			RGS22		HNF4G	
			FBXL6		HSF1	
			SNORA72		IKBKB	
			OPLAH		IL7	
			PABPC1		IMPA1	
			KCNV1		IDO1	
			MTBP		EIF3E	
			EIF2C2		KCNQ3	
			COMMD5		KCNS2	
			MRPL13		LOXL2	
			ATAD2		LPL	
			ASAP1-IT1		LY6E	
			CPSF1		LY6H	
			LRP12		LYN	
			RRM2B		MATN2	
			CYHR1		MCM4	
			ASAP1		MMP16	
			MTERFD1		MOS	
			FAM135B		MSR1	
			PHF20L1		MSRA	
			ZNF706		MYBL1	
			VPS28		MYC	
			FAM203A		NBN	
			KCNK9		NDUFB9	
			C8orf55		NEFM	
			UBR5		NEFL	
			FAM49B		TONSL	
			AZIN1		NKX3-1	
			CHRAC1		NOV	
			EXOSC4		ODF1	
			LY6K		TNFRSF11B	
			TRMT12		OPRK1	
			OXR1		PCM1	
			WDYHV1		PDE7A	
			LAPTM4B		PDGFRL	
			SLC39A4		ENPP2	
			SYBU		PENK	
			GSDMC		PLAG1	
			ENY2		PLAT	
			SLURP1		PLEC	
			SLC45A4		PNOC	
			ZFAT		PMP2	
			ZNF250		POLB	
			DEPTOR		POLR2K	
			PYCRL		POU5F1B	
			C8orf33		PPP2CB	
			LYNX1		PPP2R2A	
			C8orf51		PPP3CC	
			DSCC1		PKIA	
			DERL1		PRKDC	
			GPR172A		PTK2	
			PLEKHF2		PVT1	
			GSDMD		PEX2	
			NIPAL2		RAB2A	
			BAALC		RAD21	
			ZNF696		RP1	
			GRHL2		RPL7	
			ARHGAP39		RPL8	
			ZNF34		RPL30	
			SLC25A32		RPS20	
			TM7SF4		SDC2	
			SHARPIN		SDCBP	
			EPPK1		SFRP1	
			SCRT1		SFTPC	
			TRAPPC9		ST3GAL1	
			TATDN1		SLA	
			NACAP1		SLC7A2	
			NCALD		SLC18A1	
			MAF1		SLC20A2	
			UTP23		SNAI2	
			PARP10		SNTB1	
			C8orf76		SPAG1	
			TIGD5		SQLE	
			NUDCD1		STAR	
			FAM83A		STC1	
			PPP1R16A		STK3	
			TSPYL5		TACC1	
			MED30		TAF2	
			ZNF251		TCEA1	
			KIFC2		TCEB1	
			MTDH		TERF1	
			PKHD1L1		TG	
			NAPRT1		KLF10	
			WDR67		TPD52	
			HPYR1		TRHR	
			MFSD3		TRPS1	
			MAL2		TSTA3	
			CSMD3		TTPA	
			RHPN1		UBE2V2	
			FBXO32		COL14A1	
			CTHRC1		UQCRB	
			OSR2		VDAC3	
			TOP1MT		WRN	
			ZNF572		YWHAZ	
			C8orf38		ZNF7	
			ABRA		ZNF16	
			LYPD2		FZD3	
			TMEM71		TUSC3	
			TMEM65		UBXN8	
			LOC157381		KAT6A	
			C8orf56		PSCA	
			ANKRD46		FZD6	
			FAM84B		NSMAF	
			C8orf37		LY6D	
			VPS13B		RGS20	
			TMEM74		JRK	
			FAM91A1		TNKS	
			SLC30A8		EIF3H	
			COL22A1		DGAT1	
			SNX31		GPAA1	
			ADCK5		ADAM18	
			TSNARE1		ADAM9	
			C8orf47		ADAM7	
			MAPK15		RIPK2	
			NSMCE2		TNFRSF10D	
			ZNF707		TNFRSF10C	
			BREA2		TNFRSF10B	
			FAM83H		TNFRSF10A	
			LOC286094		FGF17	
			ZNF252		GGH	
			TMED10P1		WISP1	
			C8orf77		CPNE3	
			C8orf31		FOXH1	
			ZFP41		CHRNA6	
			FBXO43		TRPA1	
			GPIHBP1		DOK2	
			KLHL38		ASH2L	
			NRBP2		MTMR7	
			ZNF517		CCNE2	
			KIAA1875		EBAG9	
			C8ORFK29		MYOM2	
			RSPO2		DLGAP2	
			SPATC1		MSC	
			FLJ43860		MFHAS1	
			MAFA		KCNB2	
			GDF6		RECQL4	
			SAMD12		CYP7B1	
			MIR30B		BAG4	
			MIR30D		ENTPD4	
			C8orf82		ARHGEF10	
			FER1L6-AS1		MTFR1	
			FLJ42969		LRRC14	
			C8orf85		TTC35	
			LRRC24		RIMS2	
			SAMD12-AS1		ST18	
			ZFAT-AS1		TOX	
			HAS2-AS1		MTSS1	
			C8orf69		PTDSS1	
			LINC00051		PHYHIP	
			C8orf73		RB1CC1	
			SCXB		ZNF623	
			RAD21-AS1		KIAA0196	
			FER1L6		KBTBD11	
			MIR599		HHLA1	
			MIR661		SORBS3	
			LOC727677		TRIB1	
			HEATR7A		HRSP12	
			LOC728724		NPM2	
			OC90		DLC1	
			LOC731779		NDRG1	
			MIR875		PGCP	
			MIR937		SPAG11B	
			MIR939		LYPLA1	
			LOC100128338		NCOA2	
			SCXA		ARFGEF1	
			LOC100130231		COLEC10	
			CCDC166		KHDRBS3	
			LOC100131726		DCTN6	
			LOC100133669		PNMA2	
			LOC100288181		ADAM28	
			MIR1205		POP1	
			MIR1206		AP3M2	
			MIR1207		COPS5	
			MIR1204		RBPMS	
			MIR1234		WWP1	
			MIR2053		STMN2	
			MIR1208		PTP4A3	
			MIR3150A		ERLIN2	
			MIR3151		LZTS1	
			LOC100499183		PROSC	
			LOC100500773		RNF139	
			MIR3150B		ZHX1	
			MIR3610		PUF60	
			LOC100507117		ZHX2	
			ZHX1-C8ORF76		DENND3	
			MIR4663		XPO7	
			MIR4472-1		TRIM35	
			MIR4664		ZC3H3	
			MIR4471		EFR3A	
			LOC100616530		RRS1	
			PCAT1		SULF1	
			LINC00536		RHOBTB2	
					ARC	
					BOP1	
					DDHD2	
					KIF13B	
					PSD3	
					ZFPM2	
					HEY1	
					TRAM1	
					LEPROTL1	
					SCRIB	
					KIAA0146	
					SLC39A14	
					LRRC6	
					LY96	
					SGK3	
					RAD54B	
					DCAF13	
					RNF19A	
					GPR124	
					KIAA1429	
					C8orf71	
					RGS22	
					FBXL6	
					PTTG3P	
					FBXO25	
					FGF20	
					SNORA72	
					SNORD54	
					OPLAH	
					PABPC1	
					KCNV1	
					STAU2	
					MTBP	
					DKK4	
					EIF2C2	
					LSM1	
					ADAMDEC1	
					BHLHE22	
					MRPS28	
					COMMD5	
					MRPL13	
					ATAD2	
					ASAP1-IT1	
					MRPL15	
					CNOT7	
					CPSF1	
					PURG	
					LRP12	
					RRM2B	
					CYHR1	
					ASAP1	
					MTERFD1	
					PI15	
					FAM135B	
					ZC2HC1A	
					PHF20L1	
					LACTB2	
					FAM82B	
					ZNF706	
					GOLGA7	
					VPS28	
					ZDHHC2	
					FAM203A	
					KCNK9	
					SLC25A37	
					C8orf55	
					UBR5	
					SCARA3	
					FAM49B	
					AZIN1	
					ATP6V1H	
					OTUD6B	
					TMEM66	
					CHRAC1	
					SNTG1	
					GDAP1	
					EXOSC4	
					PDP1	
					CNGB3	
					LY6K	
					KCTD9	
					ESRP1	
					WHSC1L1	
					IMPAD1	
					TMEM70	
					PINX1	
					TRMT12	
					OXR1	
					WDYHV1	
					PIWIL2	
					ELP3	
					THAP1	
					ARMC1	
					INTS10	
					CCDC25	
					UBE2W	
					BRF2	
					AGPAT5	
					LAPTM4B	
					C8orf39	
					TMEM55A	
					SLC39A4	
					CHD7	
					SYBU	
					INTS8	
					INTS9	
					CSGALNACT1	
					HR	
					PAG1	
					PBK	
					ZNF395	
					DEFB103B	
					BIN3	
					TEX15	
					GSDMC	
					C8orf44	
					JPH1	
					C8orf4	
					ENY2	
					CPA6	
					SLURP1	
					SLC45A4	
					MTUS1	
					KIAA1456	
					ZFAT	
					KIAA1967	
					ZNF250	
					PLEKHA2	
					SH2D4A	
					PRDM14	
					SNX16	
					NECAB1	
					PDLIM2	
					SOX17	
					CSMD1	
					EBF2	
					FAM160B2	
					DEPTOR	
					PYCRL	
					C8orf33	
					ZBTB10	
					LYNX1	
					MTMR9	
					DUSP26	
					C8orf51	
					DSCC1	
					DERL1	
					CHCHD7	
					GPR172A	
					HMBOX1	
					EFCAB1	
					MCPH1	
					PPP1R3B	
					PLEKHF2	
					ZMAT4	
					ZFAND1	
					ZFHX4	
					GSDMD	
					NIPAL2	
					RNF122	
					CSPP1	
					BAALC	
					NUDT18	
					ZNF696	
					GRHL2	
					DOCK5	
					FLJ14107	
					VCPIP1	
					ZNF703	
					TTI2	
					RAB11FIP1	
					PREX2	
					REEP4	
					ARHGAP39	
					ZNF34	
					SLC25A32	
					TM7SF4	
					STMN4	
					RNF170	
					SLCO5A1	
					SHARPIN	
					EPPK1	
					SCRT1	
					SOX7	
					FAM167A	
					SLC35G5	
					LINC00208	
					C8orf12	
					CRISPLD1	
					TRAPPC9	
					TM2D2	
					TATDN1	
					NACAP1	
					NCALD	
					SGK196	
					MAF1	
					UTP23	
					GINS4	
					HOOK3	
					PPAPDC1B	
					MAK16	
					TRIM55	
					FUT10	
					PARP10	
					C8orf76	
					TIGD5	
					NUDCD1	
					FAM83A	
					PPP1R16A	
					FAM86B1	
					LRRCC1	
					TSPYL5	
					DNAJC5B	
					PSKH2	
					FAM110B	
					MED30	
					ERI1	
					ZNF251	
					KIFC2	
					TMEM67	
					LONRF1	
					CHMP7	
					MTDH	
					CHMP4C	
					PKHD1L1	
					NAPRT1	
					WDR67	
					HPYR1	
					RP1L1	
					TP53INP1	
					TGS1	
					MFSD3	
					MAL2	
					XKR4	
					CSMD3	
					RHPN1	
					FBXO32	
					C8orf40	
					SLC26A7	
					PCMTD1	
					CTHRC1	
					OSR2	
					C8orf34	
					TOP1MT	
					CLDN23	
					ZNF572	
					GOT1L1	
					FAM92A1	
					VPS37A	
					C8orf38	
					TMEM68	
					ABRA	
					LYPD2	
					NKX2-6	
					TMEM71	
					SGCZ	
					ADHFE1	
					UBXN2B	
					PXDNL	
					AGPAT6	
					UNC5D	
					LETM2	
					DCAF4L2	
					RALYL	
					HGSNAT	
					DEFB104A	
					LOC157273	
					SGK223	
					PEBP4	
					CDCA2	
					TMEM65	
					LOC157381	
					RDH10	
					C8orf56	
					ANKRD46	
					ESCO2	
					FBXO16	
					LOC157627	
					FAM84B	
					C8orf37	
					VPS13B	
					C8orf42	
					ERICH1	
					SLC7A13	
					TDH	
					TMEM74	
					FAM91A1	
					C8orf48	
					C8orf45	
					CLVS1	
					NKX6-3	
					KCNU1	
					C8orf84	
					CNBD1	
					SLC30A8	
					COL22A1	
					SNX31	
					TMEM64	
					ZNF596	
					IDO2	
					DEFT1P	
					SDR16C5	
					ADCK5	
					TSNARE1	
					R3HCC1	
					PRSS55	
					C8orf74	
					HTRA4	
					ADAM32	
					C8orf47	
					LGI3	
					MAPK15	
					DEFB105A	
					DEFB106A	
					DEFB107A	
					DEFB109P1	
					DEFB130	
					ATP6V0D2	
					NEIL2	
					YTHDF3	
					C8orf46	
					LOC254896	
					REXO1L1	
					ADAM5P	
					FLJ10661	
					XKR6	
					NSMCE2	
					LOC286059	
					ZNF707	
					BREA2	
					FAM83H	
					LOC286083	
					LOC286094	
					EFHA2	
					ZNF252	
					TMED10P1	
					C8orf77	
					LOC286114	
					C8orf31	
					ZFP41	
					SCARA5	
					LOC286135	
					RNF5P1	
					C8orf83	
					DPY19L4	
					FBXO43	
					LOC286177	
					NKAIN3	
					LOC286184	
					LOC286186	
					PPP1R42	
					LOC286189	
					LOC286190	
					GPIHBP1	
					LOC340357	
					KLHL38	
					NRBP2	
					ZNF517	
					KIAA1875	
					C8ORFK29	
					RSPO2	
					POTEA	
					SLC10A5	
					LOC349196	
					SPATC1	
					USP17L2	
					CA13	
					XKR5	
					FAM90A25P	
					LOC389641	
					C8orf80	
					C8orf86	
					FAM150A	
					XKR9	
					LOC389676	
					RBM12B	
					FLJ43860	
					MAFA	
					LOC392196	
					LOC392232	
					GDF6	
					LOC401463	
					C8orf59	
					SAMD12	
					MIR124-1	
					MIR124-2	
					MIR30B	
					MIR30D	
					MIR320A	
					DEFB103A	
					C8orf82	
					FER1L6-AS1	
					OR4F21	
					FAM90A13	
					FAM90A5	
					FAM90A7	
					FAM90A8	
					FAM90A18	
					FAM90A9	
					FAM90A10	
					FLJ39080	
					FLJ46284	
					FLJ42969	
					C8orf85	
					LRRC24	
					DEFA10P	
					C8orf22	
					MIR383	
					LINC00293	
					DEFB107B	
					DEFB104B	
					DEFB106B	
					DEFB105B	
					C8orf58	
					LINC00251	
					SAMD12-AS1	
					ZFAT-AS1	
					HAS2-AS1	
					DEFB135	
					DEFB136	
					DEFB134	
					ZNF704	
					C8orf69	
					C8orf75	
					MBOAT4	
					LINC00051	
					MIR486	
					DEFB109P1B	
					SNHG6	
					SNORD87	
					C8orf73	
					SCXB	
					LINC00535	
					UG0898H09	
					RPL23AP53	
					RAD21-AS1	
					FAM90A14	
					FABP9	
					FABP12	
					FAM86B2	
					SPAG11A	
					FER1L6	
					MIR596	
					MIR597	
					MIR598	
					MIR599	
					MIR661	
					LOC727677	
					HEATR7A	
					LOC728024	
					DEFA1B	
					FAM90A20	
					LOC728724	
					FAM90A19	
					ZNF705D	
					OC90	
					LOC731779	
					MIR875	
					MIR937	
					MIR939	
					LOC100127983	
					LOC100128126	
					LOC100128338	
					LOC100128750	
					FAM66B	
					LOC100128993	
					TCF24	
					SCXA	
					LOC100130155	
					LOC100130231	
					CCDC166	
					LOC100130298	
					LOC100130301	
					LRRC69	
					LOC100130964	
					LOC100131726	
					ZNF705G	
					FAM66E	
					LOC100132396	
					LOC100132891	
					FAM66D	
					FAM66A	
					SBF1P1	
					LOC100133267	
					LOC100133669	
					LOC100192378	
					LOC100287015	
					DEFT1P2	
					LOC100287846	
					LOC100288181	
					REXO1L2P	
					LOC100288748	
					DEFB4B	
					MIR1205	
					MIR1322	
					MIR1206	
					MIR1207	
					MIR1204	
					MIR548I3	
					MIR1234	
					MIR2053	
					MIR2052	
					MIR1208	
					MIR4287	
					MIR3148	
					MIR4288	
					MIR3150A	
					MIR3151	
					LOC100499183	
					LOC100500773	
					MIR3926-2	
					MIR3622A	
					MIR3926-1	
					MIR3622B	
					MIR3150B	
					MIR3610	
					LOC100505659	
					LOC100505676	
					LOC100505718	
					LOC100506990	
					LOC100507117	
					LOC100507156	
					LOC100507341	
					LOC100507632	
					LOC100507651	
					C8orf44-SGK3	
					ZHX1-C8ORF76	
					MIR4469	
					MIR378D2	
					MIR548O2	
					MIR4661	
					MIR4663	
					MIR4472-1	
					MIR4664	
					MIR4659A	
					MIR4660	
					MIR4659B	
					MIR4471	
					MIR4470	
					LOC100616530	
					LOC100652791	
					PCAT1	
					LINC00536	
					FSBP	
